import { ICarte } from 'app/shared/model//carte.model';
import { IOuverture } from 'app/shared/model//ouverture.model';
import { IType } from 'app/shared/model//type.model';
import { IServeur } from 'app/shared/model//serveur.model';
import { IServices } from 'app/shared/model//services.model';
import { IGestionnaire } from 'app/shared/model//gestionnaire.model';

export interface IRestaurant {
    id?: number;
    nom?: string;
    adresse?: string;
    capacite?: number;
    latitude?: number;
    longitude?: number;
    nbtables?: number;
    carte?: ICarte;
    ouvertures?: IOuverture[];
    types?: IType[];
    serveur?: IServeur;
    services?: IServices[];
    gestionnaires?: IGestionnaire[];
}

export class Restaurant implements IRestaurant {
    constructor(
        public id?: number,
        public nom?: string,
        public adresse?: string,
        public capacite?: number,
        public latitude?: number,
        public longitude?: number,
        public nbtables?: number,
        public carte?: ICarte,
        public ouvertures?: IOuverture[],
        public types?: IType[],
        public serveur?: IServeur,
        public services?: IServices[],
        public gestionnaires?: IGestionnaire[]
    ) {}
}
