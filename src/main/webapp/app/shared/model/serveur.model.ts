import { IRestaurant } from 'app/shared/model//restaurant.model';
import { IReservation } from 'app/shared/model//reservation.model';

export interface IServeur {
    id?: number;
    restaurant?: IRestaurant;
    reservations?: IReservation[];
}

export class Serveur implements IServeur {
    constructor(public id?: number, public restaurant?: IRestaurant, public reservations?: IReservation[]) {}
}
