import { IGestionnaire } from 'app/shared/model//gestionnaire.model';
import { IRestaurant } from 'app/shared/model//restaurant.model';

export interface IGestionnaire {
    id?: number;
    supervises?: IGestionnaire[];
    restaurants?: IRestaurant[];
    superviseur?: IGestionnaire;
}

export class Gestionnaire implements IGestionnaire {
    constructor(
        public id?: number,
        public supervises?: IGestionnaire[],
        public restaurants?: IRestaurant[],
        public superviseur?: IGestionnaire
    ) {}
}
