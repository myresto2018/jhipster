import { Moment } from 'moment';
import { IRestaurant } from 'app/shared/model//restaurant.model';
import { IReservation } from 'app/shared/model//reservation.model';

export const enum HoraireService {
    MATIN = 'MATIN',
    MIDI = 'MIDI',
    SOIR = 'SOIR'
}

export interface IServices {
    id?: number;
    date?: Moment;
    horaire?: HoraireService;
    restaurant?: IRestaurant;
    reservations?: IReservation[];
}

export class Services implements IServices {
    constructor(
        public id?: number,
        public date?: Moment,
        public horaire?: HoraireService,
        public restaurant?: IRestaurant,
        public reservations?: IReservation[]
    ) {}
}
