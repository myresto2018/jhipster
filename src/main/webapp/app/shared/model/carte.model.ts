import { IMenu } from 'app/shared/model//menu.model';
import { IPlat } from 'app/shared/model//plat.model';
import { IRestaurant } from 'app/shared/model//restaurant.model';

export interface ICarte {
    id?: number;
    menus?: IMenu[];
    plats?: IPlat[];
    restaurant?: IRestaurant;
}

export class Carte implements ICarte {
    constructor(public id?: number, public menus?: IMenu[], public plats?: IPlat[], public restaurant?: IRestaurant) {}
}
