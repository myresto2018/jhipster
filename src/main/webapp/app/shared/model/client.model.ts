import { IReservation } from 'app/shared/model//reservation.model';

export interface IClient {
    id?: number;
    reservations?: IReservation[];
}

export class Client implements IClient {
    constructor(public id?: number, public reservations?: IReservation[]) {}
}
