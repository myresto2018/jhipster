import { ICarte } from 'app/shared/model//carte.model';
import { IMenu } from 'app/shared/model//menu.model';

export const enum Categorie {
    PLAT = 'PLAT',
    BOISSON = 'BOISSON',
    DESSERT = 'DESSERT',
    ENTREE = 'ENTREE'
}

export interface IPlat {
    id?: number;
    nom?: string;
    description?: string;
    type?: Categorie;
    prix?: number;
    carte?: ICarte;
    menus?: IMenu[];
}

export class Plat implements IPlat {
    constructor(
        public id?: number,
        public nom?: string,
        public description?: string,
        public type?: Categorie,
        public prix?: number,
        public carte?: ICarte,
        public menus?: IMenu[]
    ) {}
}
