import { IServices } from 'app/shared/model//services.model';
import { ITables } from 'app/shared/model//tables.model';
import { IServeur } from 'app/shared/model//serveur.model';
import { IClient } from 'app/shared/model//client.model';

export interface IReservation {
    id?: number;
    nbCouverts?: number;
    services?: IServices;
    tables?: ITables[];
    serveur?: IServeur;
    client?: IClient;
    color?: string;
}

export class Reservation implements IReservation {
    constructor(
        public id?: number,
        public nbCouverts?: number,
        public services?: IServices,
        public tables?: ITables[],
        public serveur?: IServeur,
        public client?: IClient,
        public color?: string
    ) {}
}
