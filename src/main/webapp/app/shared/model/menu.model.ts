import { IPlat } from 'app/shared/model//plat.model';
import { ICarte } from 'app/shared/model//carte.model';

export interface IMenu {
    id?: number;
    nom?: string;
    description?: string;
    price?: number;
    plats?: IPlat[];
    carte?: ICarte;
}

export class Menu implements IMenu {
    constructor(
        public id?: number,
        public nom?: string,
        public description?: string,
        public price?: number,
        public plats?: IPlat[],
        public carte?: ICarte
    ) {}
}
