import { IRestaurant } from 'app/shared/model//restaurant.model';

export enum TypeRestaurant {
    FASTFOOD = 'FASTFOOD',
    PIZZERIA = 'PIZZERIA',
    BURGER = 'BURGER',
    GASTRONOMIQUE = 'GASTRONOMIQUE',
    INDIEN = 'INDIEN',
    CHINOIS = 'CHINOIS',
    TRADITIONNEL = 'TRADITIONNEL',
    SAVOYARD = 'SAVOYARD'
}

export interface IType {
    id?: number;
    typeu?: TypeRestaurant;
    restaurant?: IRestaurant;
}

export class Type implements IType {
    constructor(public id?: number, public typeu?: TypeRestaurant, public restaurant?: IRestaurant) {}
}
