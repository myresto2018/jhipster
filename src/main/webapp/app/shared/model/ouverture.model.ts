import { IRestaurant } from 'app/shared/model//restaurant.model';

export const enum Jour {
    LUNDI = 'LUNDI',
    MARDI = 'MARDI',
    MERCREDI = 'MERCREDI',
    JEUDI = 'JEUDI',
    VENDREDI = 'VENDREDI',
    SAMEDI = 'SAMEDI',
    DIMANCHE = 'DIMANCHE'
}

export const enum HoraireService {
    MATIN = 'MATIN',
    MIDI = 'MIDI',
    SOIR = 'SOIR'
}

export interface IOuverture {
    id?: number;
    day?: Jour;
    creneau?: HoraireService;
    restaurant?: IRestaurant;
}

export class Ouverture implements IOuverture {
    constructor(public id?: number, public day?: Jour, public creneau?: HoraireService, public restaurant?: IRestaurant) {}
}
