import { IReservation } from 'app/shared/model//reservation.model';

export interface ITables {
    id?: number;
    x?: number;
    y?: number;
    reservations?: IReservation[];
}

export class Tables implements ITables {
    constructor(public id?: number, public x?: number, public y?: number, public reservations?: IReservation[]) {}
}
