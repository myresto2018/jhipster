import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Ouverture } from 'app/shared/model/ouverture.model';
import { OuvertureService } from './ouverture.service';
import { OuvertureComponent } from './ouverture.component';
import { OuvertureDetailComponent } from './ouverture-detail.component';
import { OuvertureUpdateComponent } from './ouverture-update.component';
import { OuvertureDeletePopupComponent } from './ouverture-delete-dialog.component';
import { IOuverture } from 'app/shared/model/ouverture.model';

@Injectable({ providedIn: 'root' })
export class OuvertureResolve implements Resolve<IOuverture> {
    constructor(private service: OuvertureService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Ouverture> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service
                .find(id)
                .pipe(
                    filter((response: HttpResponse<Ouverture>) => response.ok),
                    map((ouverture: HttpResponse<Ouverture>) => ouverture.body)
                );
        }
        return of(new Ouverture());
    }
}

export const ouvertureRoute: Routes = [
    {
        path: 'ouverture',
        component: OuvertureComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'myrestoApp.ouverture.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'ouverture/:id/view',
        component: OuvertureDetailComponent,
        resolve: {
            ouverture: OuvertureResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'myrestoApp.ouverture.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'ouverture/new',
        component: OuvertureUpdateComponent,
        resolve: {
            ouverture: OuvertureResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'myrestoApp.ouverture.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'ouverture/:id/edit',
        component: OuvertureUpdateComponent,
        resolve: {
            ouverture: OuvertureResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'myrestoApp.ouverture.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const ouverturePopupRoute: Routes = [
    {
        path: 'ouverture/:id/delete',
        component: OuvertureDeletePopupComponent,
        resolve: {
            ouverture: OuvertureResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'myrestoApp.ouverture.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
