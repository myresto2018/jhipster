import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IOuverture } from 'app/shared/model/ouverture.model';
import { Principal } from 'app/core';
import { OuvertureService } from './ouverture.service';

@Component({
    selector: 'jhi-ouverture',
    templateUrl: './ouverture.component.html'
})
export class OuvertureComponent implements OnInit, OnDestroy {
    ouvertures: IOuverture[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private ouvertureService: OuvertureService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.ouvertureService.query().subscribe(
            (res: HttpResponse<IOuverture[]>) => {
                this.ouvertures = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInOuvertures();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IOuverture) {
        return item.id;
    }

    registerChangeInOuvertures() {
        this.eventSubscriber = this.eventManager.subscribe('ouvertureListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
