import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IOuverture } from 'app/shared/model/ouverture.model';

type EntityResponseType = HttpResponse<IOuverture>;
type EntityArrayResponseType = HttpResponse<IOuverture[]>;

@Injectable({ providedIn: 'root' })
export class OuvertureService {
    public resourceUrl = SERVER_API_URL + 'api/ouvertures';

    constructor(private http: HttpClient) {}

    create(ouverture: IOuverture): Observable<EntityResponseType> {
        return this.http.post<IOuverture>(this.resourceUrl, ouverture, { observe: 'response' });
    }

    update(ouverture: IOuverture): Observable<EntityResponseType> {
        return this.http.put<IOuverture>(this.resourceUrl, ouverture, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IOuverture>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IOuverture[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
