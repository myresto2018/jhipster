import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MyrestoSharedModule } from 'app/shared';
import {
    OuvertureComponent,
    OuvertureDetailComponent,
    OuvertureUpdateComponent,
    OuvertureDeletePopupComponent,
    OuvertureDeleteDialogComponent,
    ouvertureRoute,
    ouverturePopupRoute
} from './';

const ENTITY_STATES = [...ouvertureRoute, ...ouverturePopupRoute];

@NgModule({
    imports: [MyrestoSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        OuvertureComponent,
        OuvertureDetailComponent,
        OuvertureUpdateComponent,
        OuvertureDeleteDialogComponent,
        OuvertureDeletePopupComponent
    ],
    entryComponents: [OuvertureComponent, OuvertureUpdateComponent, OuvertureDeleteDialogComponent, OuvertureDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MyrestoOuvertureModule {}
