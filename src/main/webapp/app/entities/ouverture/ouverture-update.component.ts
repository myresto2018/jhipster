import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IOuverture } from 'app/shared/model/ouverture.model';
import { OuvertureService } from './ouverture.service';
import { IRestaurant } from 'app/shared/model/restaurant.model';
import { RestaurantService } from 'app/entities/restaurant';

@Component({
    selector: 'jhi-ouverture-update',
    templateUrl: './ouverture-update.component.html'
})
export class OuvertureUpdateComponent implements OnInit {
    ouverture: IOuverture;
    isSaving: boolean;

    restaurants: IRestaurant[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private ouvertureService: OuvertureService,
        private restaurantService: RestaurantService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ ouverture }) => {
            this.ouverture = ouverture;
        });
        this.restaurantService.query().subscribe(
            (res: HttpResponse<IRestaurant[]>) => {
                this.restaurants = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.ouverture.id !== undefined) {
            this.subscribeToSaveResponse(this.ouvertureService.update(this.ouverture));
        } else {
            this.subscribeToSaveResponse(this.ouvertureService.create(this.ouverture));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IOuverture>>) {
        result.subscribe((res: HttpResponse<IOuverture>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackRestaurantById(index: number, item: IRestaurant) {
        return item.id;
    }
}
