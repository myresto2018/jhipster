import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IOuverture } from 'app/shared/model/ouverture.model';
import { OuvertureService } from './ouverture.service';

@Component({
    selector: 'jhi-ouverture-delete-dialog',
    templateUrl: './ouverture-delete-dialog.component.html'
})
export class OuvertureDeleteDialogComponent {
    ouverture: IOuverture;

    constructor(private ouvertureService: OuvertureService, public activeModal: NgbActiveModal, private eventManager: JhiEventManager) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.ouvertureService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'ouvertureListModification',
                content: 'Deleted an ouverture'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-ouverture-delete-popup',
    template: ''
})
export class OuvertureDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ ouverture }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(OuvertureDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.ouverture = ouverture;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
