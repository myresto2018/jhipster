import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IOuverture } from 'app/shared/model/ouverture.model';

@Component({
    selector: 'jhi-ouverture-detail',
    templateUrl: './ouverture-detail.component.html'
})
export class OuvertureDetailComponent implements OnInit {
    ouverture: IOuverture;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ ouverture }) => {
            this.ouverture = ouverture;
        });
    }

    previousState() {
        window.history.back();
    }
}
