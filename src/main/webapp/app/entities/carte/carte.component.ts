import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ICarte } from 'app/shared/model/carte.model';
import { Principal } from 'app/core';
import { CarteService } from './carte.service';

@Component({
    selector: 'jhi-carte',
    templateUrl: './carte.component.html'
})
export class CarteComponent implements OnInit, OnDestroy {
    cartes: ICarte[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private carteService: CarteService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.carteService.query().subscribe(
            (res: HttpResponse<ICarte[]>) => {
                this.cartes = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInCartes();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ICarte) {
        return item.id;
    }

    registerChangeInCartes() {
        this.eventSubscriber = this.eventManager.subscribe('carteListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
