import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IServeur } from 'app/shared/model/serveur.model';
import { ServeurService } from './serveur.service';
import { IRestaurant } from 'app/shared/model/restaurant.model';
import { RestaurantService } from 'app/entities/restaurant';

@Component({
    selector: 'jhi-serveur-update',
    templateUrl: './serveur-update.component.html'
})
export class ServeurUpdateComponent implements OnInit {
    serveur: IServeur;
    isSaving: boolean;

    restaurants: IRestaurant[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private serveurService: ServeurService,
        private restaurantService: RestaurantService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ serveur }) => {
            this.serveur = serveur;
        });
        this.restaurantService.query({ filter: 'serveur-is-null' }).subscribe(
            (res: HttpResponse<IRestaurant[]>) => {
                if (!this.serveur.restaurant || !this.serveur.restaurant.id) {
                    this.restaurants = res.body;
                } else {
                    this.restaurantService.find(this.serveur.restaurant.id).subscribe(
                        (subRes: HttpResponse<IRestaurant>) => {
                            this.restaurants = [subRes.body].concat(res.body);
                        },
                        (subRes: HttpErrorResponse) => this.onError(subRes.message)
                    );
                }
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.serveur.id !== undefined) {
            this.subscribeToSaveResponse(this.serveurService.update(this.serveur));
        } else {
            this.subscribeToSaveResponse(this.serveurService.create(this.serveur));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IServeur>>) {
        result.subscribe((res: HttpResponse<IServeur>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackRestaurantById(index: number, item: IRestaurant) {
        return item.id;
    }
}
