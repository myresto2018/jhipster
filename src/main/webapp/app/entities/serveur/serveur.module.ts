import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MyrestoSharedModule } from 'app/shared';
import {
    ServeurComponent,
    ServeurDetailComponent,
    ServeurUpdateComponent,
    ServeurDeletePopupComponent,
    ServeurDeleteDialogComponent,
    serveurRoute,
    serveurPopupRoute
} from './';

const ENTITY_STATES = [...serveurRoute, ...serveurPopupRoute];

@NgModule({
    imports: [MyrestoSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        ServeurComponent,
        ServeurDetailComponent,
        ServeurUpdateComponent,
        ServeurDeleteDialogComponent,
        ServeurDeletePopupComponent
    ],
    entryComponents: [ServeurComponent, ServeurUpdateComponent, ServeurDeleteDialogComponent, ServeurDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MyrestoServeurModule {}
