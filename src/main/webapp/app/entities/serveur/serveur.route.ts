import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Serveur } from 'app/shared/model/serveur.model';
import { ServeurService } from './serveur.service';
import { ServeurComponent } from './serveur.component';
import { ServeurDetailComponent } from './serveur-detail.component';
import { ServeurUpdateComponent } from './serveur-update.component';
import { ServeurDeletePopupComponent } from './serveur-delete-dialog.component';
import { IServeur } from 'app/shared/model/serveur.model';

@Injectable({ providedIn: 'root' })
export class ServeurResolve implements Resolve<IServeur> {
    constructor(private service: ServeurService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((serveur: HttpResponse<Serveur>) => serveur.body));
        }
        return of(new Serveur());
    }
}

export const serveurRoute: Routes = [
    {
        path: 'serveur',
        component: ServeurComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'myrestoApp.serveur.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'serveur/:id/view',
        component: ServeurDetailComponent,
        resolve: {
            serveur: ServeurResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'myrestoApp.serveur.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'serveur/new',
        component: ServeurUpdateComponent,
        resolve: {
            serveur: ServeurResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'myrestoApp.serveur.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'serveur/:id/edit',
        component: ServeurUpdateComponent,
        resolve: {
            serveur: ServeurResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'myrestoApp.serveur.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const serveurPopupRoute: Routes = [
    {
        path: 'serveur/:id/delete',
        component: ServeurDeletePopupComponent,
        resolve: {
            serveur: ServeurResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'myrestoApp.serveur.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
