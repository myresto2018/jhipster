import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IRestaurant } from 'app/shared/model/restaurant.model';
import { RestaurantService } from './restaurant.service';
import { ICarte } from 'app/shared/model/carte.model';
import { CarteService } from 'app/entities/carte';
import { IServeur } from 'app/shared/model/serveur.model';
import { ServeurService } from 'app/entities/serveur';
import { IGestionnaire } from 'app/shared/model/gestionnaire.model';
import { GestionnaireService } from 'app/entities/gestionnaire';

@Component({
    selector: 'jhi-restaurant-update',
    templateUrl: './restaurant-update.component.html'
})
export class RestaurantUpdateComponent implements OnInit {
    restaurant: IRestaurant;
    isSaving: boolean;

    cartes: ICarte[];

    serveurs: IServeur[];

    gestionnaires: IGestionnaire[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private restaurantService: RestaurantService,
        private carteService: CarteService,
        private serveurService: ServeurService,
        private gestionnaireService: GestionnaireService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ restaurant }) => {
            this.restaurant = restaurant;
        });
        this.carteService.query({ filter: 'restaurant-is-null' }).subscribe(
            (res: HttpResponse<ICarte[]>) => {
                if (!this.restaurant.carte || !this.restaurant.carte.id) {
                    this.cartes = res.body;
                } else {
                    this.carteService.find(this.restaurant.carte.id).subscribe(
                        (subRes: HttpResponse<ICarte>) => {
                            this.cartes = [subRes.body].concat(res.body);
                        },
                        (subRes: HttpErrorResponse) => this.onError(subRes.message)
                    );
                }
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.serveurService.query().subscribe(
            (res: HttpResponse<IServeur[]>) => {
                this.serveurs = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.gestionnaireService.query().subscribe(
            (res: HttpResponse<IGestionnaire[]>) => {
                this.gestionnaires = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.restaurant.id !== undefined) {
            this.subscribeToSaveResponse(this.restaurantService.update(this.restaurant));
        } else {
            this.subscribeToSaveResponse(this.restaurantService.create(this.restaurant));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IRestaurant>>) {
        result.subscribe((res: HttpResponse<IRestaurant>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackCarteById(index: number, item: ICarte) {
        return item.id;
    }

    trackServeurById(index: number, item: IServeur) {
        return item.id;
    }

    trackGestionnaireById(index: number, item: IGestionnaire) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}
