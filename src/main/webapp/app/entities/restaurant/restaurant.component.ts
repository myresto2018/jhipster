import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpHeaders, HttpResponse, HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { IRestaurant, Restaurant } from 'app/shared/model/restaurant.model';
import { Principal } from 'app/core';

import { ITEMS_PER_PAGE } from 'app/shared';
import { RestaurantService } from './restaurant.service';
import { type } from 'os';
import { isThisTypeNode } from 'typescript';

@Component({
    selector: 'jhi-restaurant',
    templateUrl: './restaurant.component.html'
})
export class RestaurantComponent implements OnInit, OnDestroy {
    currentAccount: any;
    restaurants: IRestaurant[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    url = 'api/restaurants/';
    public isInit = false;
    public nb = 1;
    public nbday = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    public data2: [81, 12, 50, 49, 89, 38, 60];
    public label2: 'Restaurant 2';
    public lineChartLabels = [
        'Janvier',
        'Février',
        'Mars',
        'Avril',
        'Mai',
        'Juin',
        'Juillet',
        'Août',
        'Septembre',
        'Octobre',
        'Novembre',
        'Décembre'
    ];
    public lineChartData: Array<restoId> = [
        { data: [], label: ' ', fill: false },
        { data: [], label: ' ', fill: false },
        { data: [], label: ' ', fill: false },
        { data: [], label: ' ', fill: false },
        { data: [], label: ' ', fill: false },
        { data: [], label: ' ', fill: false }
    ];

    public data: Array<restoId> = [
        //        { data: [81, 12, 50, 49, 89, 38, 60, 10, 26, 39, 45, 50], label: 'Aux 3 Copains', fill: false },
        //      { data: [90, 64, 24, 39, 72, 13, 20, 24, 35, 49, 60, 76], label: 'Le clos', fill: false },
        //    { data: new Array<number>(12), label: 'La Brasserie', fill: false }
    ];
    public lineChartType = 'line';

    constructor(
        private http: HttpClient,
        private restaurantService: RestaurantService,
        private parseLinks: JhiParseLinks,
        private jhiAlertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe(data => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
    }

    loadAll() {
        this.restaurantService
            .query({
                page: this.page - 1,
                size: this.itemsPerPage,
                sort: this.sort()
            })
            .subscribe(
                (res: HttpResponse<IRestaurant[]>) => this.paginateRestaurants(res.body, res.headers),
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/restaurant'], {
            queryParams: {
                page: this.page,
                size: this.itemsPerPage,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.router.navigate([
            '/restaurant',
            {
                page: this.page,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        ]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInRestaurants();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IRestaurant) {
        return item.id;
    }

    registerChangeInRestaurants() {
        this.eventSubscriber = this.eventManager.subscribe('restaurantListModification', response => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private paginateRestaurants(data: IRestaurant[], headers: HttpHeaders) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
        this.queryCount = this.totalItems;
        this.restaurants = data;
        this.initData();
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    public add(lbl): void {
        let dt: Array<number>;
        this.data.forEach(d => {
            if (lbl === d.label) {
                dt = d.data;
            }
        });
        if (this.nb === 6) {
            this.nb = 1;
        }
        if (!this.appartient(lbl)) {
            this.lineChartData[this.nb] = { data: dt, label: lbl, fill: false };
            this.nb++;
        }
        this.update();
    }

    public remove(lbl): void {
        let decale = false;
        if (this.appartient(lbl)) {
            for (let index = 1; index < this.lineChartData.length; index++) {
                if (decale) {
                    this.lineChartData[index - 1] = this.lineChartData[index];
                }
                if (this.lineChartData[index].label === lbl) {
                    this.lineChartData[index] = { data: [], label: '', fill: false };
                    decale = true;
                    if (this.nb === 1) {
                        this.nb = this.lineChartData.length - 1;
                    } else {
                        this.nb--;
                    }
                }
            }
        }
        this.update();
    }

    public change(lbl): void {
        if (!this.isInit) {
            this.initGraph();
            this.isInit = true;
        }
        if (this.appartient(lbl)) {
            this.remove(lbl);
        } else {
            this.add(lbl);
        }
    }

    public appartient(lbl): boolean {
        let app = false;
        this.lineChartData.forEach(data => {
            if (data.label === lbl) {
                app = true;
            }
        });
        return app;
    }

    public update(): void {
        console.log(this.lineChartData);
        this.lineChartData = this.lineChartData.slice();
    }

    public getLabel(id): string {
        let ret: string;
        this.restaurants.forEach(restaurant => {
            if (restaurant.id === id) {
                ret = restaurant.nom;
            }
        });
        return ret;
    }

    public getAPI(id) {
        const tauxresa = new Array<number>(12);
        for (let mois = 1; mois <= 12; mois++) {
            this.http
                .get<number>(
                    this.url +
                        id +
                        '/statistic?deb=2018-' +
                        this.formatmois(mois) +
                        '-01&fin=2018-' +
                        this.formatmois(mois) +
                        '-' +
                        this.nbday[mois - 1]
                )
                .subscribe(num => {
                    if (isNaN(num)) {
                        tauxresa[mois - 1] = 0;
                    } else {
                        tauxresa[mois - 1] = num * 100;
                    }
                });
        }
        console.log('getLabel : ' + this.getLabel(id));
        this.data.push({ data: tauxresa, label: this.getLabel(id), fill: false });
    }

    public formatmois(m): string {
        if (m < 10) {
            return '0' + m;
        } else {
            return m;
        }
    }

    public initData() {
        this.restaurants.forEach(restaurant => {
            this.getAPI(restaurant.id);
        });
    }

    public initGraph() {
        let moy = new Array<number>(12);
        for (let index = 0; index < moy.length; index++) {
            moy[index] = 0;
        }
        let numResto = 0;
        console.log('init Graph');
        this.restaurants.forEach(restaurant => {
            console.log(restaurant.id);
            for (let index = 0; index < 12; index++) {
                moy[index] = moy[index] + this.data[numResto].data[index];
                console.log(moy[index]);
            }
            numResto = numResto + 1;
        });
        console.log(this.data);
        moy = moy.map(x => x / this.restaurants.length);
        this.lineChartData[0] = { data: moy, label: 'Moyenne', fill: false };
    }
}

type restoId = { data: number[]; label: string; fill: boolean };
