import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IServices } from 'app/shared/model/services.model';
import { IReservation } from 'app/shared/model/reservation.model';

type EntityResponseType = HttpResponse<IServices>;
type EntityArrayResponseType = HttpResponse<IServices[]>;

@Injectable({ providedIn: 'root' })
export class ServicesService {
    public resourceUrl = SERVER_API_URL + 'api/services';

    constructor(private http: HttpClient) {}

    create(services: IServices): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(services);
        return this.http
            .post<IServices>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(services: IServices): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(services);
        return this.http
            .put<IServices>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IServices>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }
    gettingReservations(id: number): Observable<EntityArrayResponseType> {
        return this.http
            .get<IReservation[]>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IServices[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    protected convertDateFromClient(services: IServices): IServices {
        const copy: IServices = Object.assign({}, services, {
            date: services.date != null && services.date.isValid() ? services.date.format(DATE_FORMAT) : null
        });
        return copy;
    }

    protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
        if (res.body) {
            res.body.date = res.body.date != null ? moment(res.body.date) : null;
        }
        return res;
    }

    protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        if (res.body) {
            res.body.forEach((services: IServices) => {
                services.date = services.date != null ? moment(services.date) : null;
            });
        }
        return res;
    }
}
