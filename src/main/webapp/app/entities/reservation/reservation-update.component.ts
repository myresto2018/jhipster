import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IReservation } from 'app/shared/model/reservation.model';
import { ReservationService } from './reservation.service';
import { IServices } from 'app/shared/model/services.model';
import { ServicesService } from 'app/entities/services';
import { ITables } from 'app/shared/model/tables.model';
import { TablesService } from 'app/entities/tables';
import { IServeur } from 'app/shared/model/serveur.model';
import { ServeurService } from 'app/entities/serveur';
import { IClient } from 'app/shared/model/client.model';
import { ClientService } from 'app/entities/client';

@Component({
    selector: 'jhi-reservation-update',
    templateUrl: './reservation-update.component.html'
})
export class ReservationUpdateComponent implements OnInit {
    reservation: IReservation;
    isSaving: boolean;

    services: IServices[];

    tables: ITables[];

    serveurs: IServeur[];

    clients: IClient[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private reservationService: ReservationService,
        private servicesService: ServicesService,
        private tablesService: TablesService,
        private serveurService: ServeurService,
        private clientService: ClientService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ reservation }) => {
            this.reservation = reservation;
        });
        this.servicesService.query().subscribe(
            (res: HttpResponse<IServices[]>) => {
                this.services = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.tablesService.query().subscribe(
            (res: HttpResponse<ITables[]>) => {
                this.tables = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.serveurService.query().subscribe(
            (res: HttpResponse<IServeur[]>) => {
                this.serveurs = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.clientService.query().subscribe(
            (res: HttpResponse<IClient[]>) => {
                this.clients = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.reservation.id !== undefined) {
            this.subscribeToSaveResponse(this.reservationService.update(this.reservation));
        } else {
            this.subscribeToSaveResponse(this.reservationService.create(this.reservation));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IReservation>>) {
        result.subscribe((res: HttpResponse<IReservation>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackServicesById(index: number, item: IServices) {
        return item.id;
    }

    trackTablesById(index: number, item: ITables) {
        return item.id;
    }

    trackServeurById(index: number, item: IServeur) {
        return item.id;
    }

    trackClientById(index: number, item: IClient) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}
