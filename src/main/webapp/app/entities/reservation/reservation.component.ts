import { Component, OnInit, OnDestroy, AfterViewChecked } from '@angular/core';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { IReservation } from 'app/shared/model/reservation.model';
import { IServices } from 'app/shared/model/services.model';
import { Principal } from 'app/core';

import { ITEMS_PER_PAGE } from 'app/shared';
import { ReservationService } from './reservation.service';
import { ServicesService } from '../services/services.service';
import { RestaurantService } from '../restaurant/restaurant.service';

function getMousePos(canvas: HTMLCanvasElement, evt: MouseEvent) {
    const rect = canvas.getBoundingClientRect();
    return new MousePosition(evt.clientX - rect.left, evt.clientY - rect.top);
}

class MousePosition {
    x: number;
    y: number;
    constructor(x: number, y: number) {
        this.x = x;
        this.y = y;
    }
}

@Component({
    selector: 'jhi-reservation',
    templateUrl: './reservation.component.html'
})
export class ReservationComponent implements OnInit, OnDestroy, AfterViewChecked {
    currentAccount: any;
    reservations: IReservation[];
    colors: string[] = [
        '#e6194b',
        '#3cb44b',
        '#ffe119',
        '#4363d8',
        '#f58231',
        '#911eb4',
        '#46f0f0',
        '#f032e6',
        '#bcf60c',
        '#fabebe',
        '#008080',
        '#e6beff',
        '#9a6324',
        '#fffac8',
        '#800000',
        '#aaffc3',
        '#808000',
        '#ffd8b1',
        '#000075',
        '#808080',
        '#ffffff',
        '#000000'
    ];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    bw = 4000;
    bh = 4000;
    widthRatio: number;
    heightRatio: number;
    p = 0;
    cw: number = this.bw + this.p * 2 + 1;
    ch: number = this.bh + this.p * 2 + 1;
    canvas: HTMLCanvasElement;
    context: CanvasRenderingContext2D;
    caseSize = 200;
    nbCase = this.bw / this.caseSize;
    tables: string[][];
    currentColor = '#ffffff';
    serviceList: IServices[];
    serviceId: number;
    constructor(
        private reservationService: ReservationService,
        private servicesService: ServicesService,
        private parseLinks: JhiParseLinks,
        private jhiAlertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager
    ) {
        this.tables = [];
        this.reservations = [];
        for (let i = 0; i < this.nbCase; i++) {
            this.tables[i] = [];
            for (let j = 0; j < this.nbCase; j++) {
                this.tables[i][j] = '#ffffff';
            }
        }
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe(data => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
    }

    loadAll() {
        this.reservationService
            .query({
                page: this.page - 1,
                size: this.itemsPerPage,
                sort: this.sort()
            })
            .subscribe(
                (res: HttpResponse<IReservation[]>) => this.paginateReservations(res.body, res.headers),
                (res: HttpErrorResponse) => this.onError(res.message)
            );
        this.servicesService
            .query({})
            .subscribe(
                (res: HttpResponse<IServices[]>) => this.handleServices(res.body),
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }
    handleServices(data: IServices[]) {
        this.serviceList = data;
        console.log(this.serviceList);
    }
    initSchema() {
        for (let i = 0; i < this.nbCase; i++) {
            this.tables[i] = [];
            for (let j = 0; j < this.nbCase; j++) {
                this.tables[i][j] = '#ffffff';
            }
        }
    }

    serviceSelected(service: IServices) {
        this.initSchema();
        this.drawBoard();
        this.reservations = [];
        console.log('Service id n°' + service.id + 'selected');
        this.serviceId = service.id;
        this.loadAll();
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/reservation'], {
            queryParams: {
                page: this.page,
                size: this.itemsPerPage,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.router.navigate([
            '/reservation',
            {
                page: this.page,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        ]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInReservations();
        this.drawBoard();
    }

    ngAfterViewChecked() {
        this.attributeColor();
    }
    attributeColor() {
        for (let i = 0; i < this.reservations.length; i++) {
            const colorizedSpan = document.getElementById('reservationn' + this.reservations[i].id);
            console.log('reservationn' + this.reservations[i].id);
            colorizedSpan.style.backgroundColor = this.reservations[i].color;
        }
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IReservation) {
        return item.id;
    }

    registerChangeInReservations() {
        this.eventSubscriber = this.eventManager.subscribe('reservationListModification', response => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private paginateReservations(data: IReservation[], headers: HttpHeaders) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
        this.queryCount = this.totalItems;
        let j = 0;
        for (let i = 0; i < this.queryCount; i++) {
            if (data[i].services.id === this.serviceId) {
                this.reservations[j] = data[i];
                j++;
            }
        }
        console.log(this.reservations);
        for (let i = 0; i < j; i++) {
            this.reservations[i].color = this.colors[i];
        }
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    private drawBoard() {
        const canvas = <HTMLCanvasElement>document.getElementById('restaurantMap');
        const div = document.getElementById('restaurantMapDiv');
        canvas.width = div.offsetWidth;
        canvas.height = div.offsetWidth;
        const canvasWidth = canvas.width;
        const canvasHeight = canvas.height;
        this.heightRatio = canvasHeight / this.bh;
        const heightRatio = this.heightRatio;
        this.widthRatio = canvasWidth / this.bw;
        const widthRatio = this.widthRatio;
        this.context = canvas.getContext('2d');
        for (let x = 0; x <= this.bw * widthRatio; x += this.caseSize * widthRatio) {
            this.context.lineWidth = 1;
            this.context.moveTo(0 + x + this.p, this.p);
            this.context.lineTo(0 + x + this.p, this.bh + this.p);
        }

        for (let y = 0; y <= this.bh * heightRatio; y += this.caseSize * heightRatio) {
            this.context.lineWidth = 1;
            this.context.moveTo(this.p, 0 + y + this.p);
            this.context.lineTo(this.bw + this.p, 0 + y + this.p);
        }
        this.nbCase = this.bw / this.caseSize;
        for (let i = 0; i < this.nbCase; i++) {
            for (let j = 0; j < this.nbCase; j++) {
                if (this.tables[i][j] !== '') {
                    this.context.fillStyle = this.tables[i][j];
                    this.context.fillRect(
                        j * this.caseSize * this.widthRatio,
                        i * this.caseSize * this.heightRatio,
                        this.caseSize * this.widthRatio,
                        this.caseSize * this.heightRatio
                    );
                }
            }
        }
        this.context.strokeStyle = 'black';
        this.context.stroke();
    }
    public resizedWindows() {
        const div = document.getElementById('restaurantMapDiv');
        const canvasWidth = div.offsetWidth;
        const canvasHeight = div.offsetWidth;
        this.heightRatio = canvasHeight / this.bh;
        this.widthRatio = canvasWidth / this.bw;
        this.drawBoard();
    }
    public clickOnCanvas(e: MouseEvent) {
        const canvas = <HTMLCanvasElement>document.getElementById('restaurantMap');
        const context = canvas.getContext('2d');
        const mousePosition: MousePosition = getMousePos(canvas, e);
        const column = Math.floor(mousePosition.y / (this.caseSize * this.widthRatio));
        const row = Math.floor(mousePosition.x / (this.caseSize * this.heightRatio));
        if (this.tables[column][row] !== '#ffffff') {
            this.tables[column][row] = '#ffffff';
        } else {
            this.tables[column][row] = this.currentColor;
        }
        console.log(this.tables);
        this.drawBoard();
    }
    public reservationSelected(reservation: IReservation) {
        this.currentColor = reservation.color;
    }
}
