import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Plat } from 'app/shared/model/plat.model';
import { PlatService } from './plat.service';
import { PlatComponent } from './plat.component';
import { PlatDetailComponent } from './plat-detail.component';
import { PlatUpdateComponent } from './plat-update.component';
import { PlatDeletePopupComponent } from './plat-delete-dialog.component';
import { IPlat } from 'app/shared/model/plat.model';

@Injectable({ providedIn: 'root' })
export class PlatResolve implements Resolve<IPlat> {
    constructor(private service: PlatService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((plat: HttpResponse<Plat>) => plat.body));
        }
        return of(new Plat());
    }
}

export const platRoute: Routes = [
    {
        path: 'plat',
        component: PlatComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'myrestoApp.plat.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'plat/:id/view',
        component: PlatDetailComponent,
        resolve: {
            plat: PlatResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'myrestoApp.plat.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'plat/new',
        component: PlatUpdateComponent,
        resolve: {
            plat: PlatResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'myrestoApp.plat.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'plat/:id/edit',
        component: PlatUpdateComponent,
        resolve: {
            plat: PlatResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'myrestoApp.plat.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const platPopupRoute: Routes = [
    {
        path: 'plat/:id/delete',
        component: PlatDeletePopupComponent,
        resolve: {
            plat: PlatResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'myrestoApp.plat.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
