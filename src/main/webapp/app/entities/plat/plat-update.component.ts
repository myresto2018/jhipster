import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IPlat } from 'app/shared/model/plat.model';
import { PlatService } from './plat.service';
import { ICarte } from 'app/shared/model/carte.model';
import { CarteService } from 'app/entities/carte';
import { IMenu } from 'app/shared/model/menu.model';
import { MenuService } from 'app/entities/menu';

@Component({
    selector: 'jhi-plat-update',
    templateUrl: './plat-update.component.html'
})
export class PlatUpdateComponent implements OnInit {
    plat: IPlat;
    isSaving: boolean;

    cartes: ICarte[];

    menus: IMenu[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private platService: PlatService,
        private carteService: CarteService,
        private menuService: MenuService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ plat }) => {
            this.plat = plat;
        });
        this.carteService.query().subscribe(
            (res: HttpResponse<ICarte[]>) => {
                this.cartes = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.menuService.query().subscribe(
            (res: HttpResponse<IMenu[]>) => {
                this.menus = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.plat.id !== undefined) {
            this.subscribeToSaveResponse(this.platService.update(this.plat));
        } else {
            this.subscribeToSaveResponse(this.platService.create(this.plat));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IPlat>>) {
        result.subscribe((res: HttpResponse<IPlat>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackCarteById(index: number, item: ICarte) {
        return item.id;
    }

    trackMenuById(index: number, item: IMenu) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}
