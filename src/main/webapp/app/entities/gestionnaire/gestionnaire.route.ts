import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Gestionnaire } from 'app/shared/model/gestionnaire.model';
import { GestionnaireService } from './gestionnaire.service';
import { GestionnaireComponent } from './gestionnaire.component';
import { GestionnaireDetailComponent } from './gestionnaire-detail.component';
import { GestionnaireUpdateComponent } from './gestionnaire-update.component';
import { GestionnaireDeletePopupComponent } from './gestionnaire-delete-dialog.component';
import { IGestionnaire } from 'app/shared/model/gestionnaire.model';

@Injectable({ providedIn: 'root' })
export class GestionnaireResolve implements Resolve<IGestionnaire> {
    constructor(private service: GestionnaireService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((gestionnaire: HttpResponse<Gestionnaire>) => gestionnaire.body));
        }
        return of(new Gestionnaire());
    }
}

export const gestionnaireRoute: Routes = [
    {
        path: 'gestionnaire',
        component: GestionnaireComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'myrestoApp.gestionnaire.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'gestionnaire/:id/view',
        component: GestionnaireDetailComponent,
        resolve: {
            gestionnaire: GestionnaireResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'myrestoApp.gestionnaire.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'gestionnaire/new',
        component: GestionnaireUpdateComponent,
        resolve: {
            gestionnaire: GestionnaireResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'myrestoApp.gestionnaire.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'gestionnaire/:id/edit',
        component: GestionnaireUpdateComponent,
        resolve: {
            gestionnaire: GestionnaireResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'myrestoApp.gestionnaire.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const gestionnairePopupRoute: Routes = [
    {
        path: 'gestionnaire/:id/delete',
        component: GestionnaireDeletePopupComponent,
        resolve: {
            gestionnaire: GestionnaireResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'myrestoApp.gestionnaire.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
