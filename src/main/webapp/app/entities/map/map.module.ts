import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MyrestoSharedModule } from 'app/shared';
import { MapComponent, mapRoute } from '.';

import { LeafletModule } from '@asymmetrik/ngx-leaflet';

const ENTITY_STATES = [...mapRoute];

@NgModule({
    imports: [MyrestoSharedModule, RouterModule.forChild(ENTITY_STATES), LeafletModule],
    declarations: [MapComponent],
    entryComponents: [MapComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MyrestoMapModule {}
