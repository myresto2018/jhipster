import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Restaurant } from 'app/shared/model/restaurant.model';
import { MapService } from './map.service';
import { MapComponent } from './map.component';
import { IRestaurant } from 'app/shared/model/restaurant.model';

@Injectable({ providedIn: 'root' })
export class MapResolve implements Resolve<IRestaurant> {
    constructor(private service: MapService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Restaurant> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service
                .find(id)
                .pipe(
                    filter((response: HttpResponse<Restaurant>) => response.ok),
                    map((restaurant: HttpResponse<Restaurant>) => restaurant.body)
                );
        }
        return of(new Restaurant());
    }
}

export const mapRoute: Routes = [
    {
        path: 'map',
        component: MapComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: [],
            defaultSort: 'id,asc',
            pageTitle: 'myrestoApp.restaurant.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
