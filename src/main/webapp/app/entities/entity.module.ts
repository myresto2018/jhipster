import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { MyrestoRestaurantModule } from './restaurant/restaurant.module';
import { MyrestoCarteModule } from './carte/carte.module';
import { MyrestoPlatModule } from './plat/plat.module';
import { MyrestoMenuModule } from './menu/menu.module';
import { MyrestoServicesModule } from './services/services.module';
import { MyrestoReservationModule } from './reservation/reservation.module';
import { MyrestoTablesModule } from './tables/tables.module';
import { MyrestoClientModule } from './client/client.module';
import { MyrestoServeurModule } from './serveur/serveur.module';
import { MyrestoGestionnaireModule } from './gestionnaire/gestionnaire.module';
import { MyrestoOuvertureModule } from './ouverture/ouverture.module';
import { MyrestoTypeModule } from './type/type.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

import { MyrestoMapModule } from './map/map.module';

@NgModule({
    // prettier-ignore
    imports: [
        MyrestoRestaurantModule,
        MyrestoCarteModule,
        MyrestoPlatModule,
        MyrestoMenuModule,
        MyrestoServicesModule,
        MyrestoReservationModule,
        MyrestoTablesModule,
        MyrestoClientModule,
        MyrestoServeurModule,
        MyrestoGestionnaireModule,
        MyrestoOuvertureModule,
        MyrestoTypeModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
        MyrestoMapModule,
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MyrestoEntityModule {}
