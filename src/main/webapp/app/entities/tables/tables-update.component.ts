import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { ITables } from 'app/shared/model/tables.model';
import { TablesService } from './tables.service';
import { IReservation } from 'app/shared/model/reservation.model';
import { ReservationService } from 'app/entities/reservation';

@Component({
    selector: 'jhi-tables-update',
    templateUrl: './tables-update.component.html'
})
export class TablesUpdateComponent implements OnInit {
    tables: ITables;
    isSaving: boolean;

    reservations: IReservation[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private tablesService: TablesService,
        private reservationService: ReservationService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ tables }) => {
            this.tables = tables;
        });
        this.reservationService.query().subscribe(
            (res: HttpResponse<IReservation[]>) => {
                this.reservations = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.tables.id !== undefined) {
            this.subscribeToSaveResponse(this.tablesService.update(this.tables));
        } else {
            this.subscribeToSaveResponse(this.tablesService.create(this.tables));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ITables>>) {
        result.subscribe((res: HttpResponse<ITables>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackReservationById(index: number, item: IReservation) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}
