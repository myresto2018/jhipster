import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ITables } from 'app/shared/model/tables.model';
import { Principal } from 'app/core';
import { TablesService } from './tables.service';

@Component({
    selector: 'jhi-tables',
    templateUrl: './tables.component.html'
})
export class TablesComponent implements OnInit, OnDestroy {
    tables: ITables[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private tablesService: TablesService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.tablesService.query().subscribe(
            (res: HttpResponse<ITables[]>) => {
                this.tables = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInTables();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ITables) {
        return item.id;
    }

    registerChangeInTables() {
        this.eventSubscriber = this.eventManager.subscribe('tablesListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
