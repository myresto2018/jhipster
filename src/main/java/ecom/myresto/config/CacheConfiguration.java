package ecom.myresto.config;

import java.time.Duration;

import org.ehcache.config.builders.*;
import org.ehcache.jsr107.Eh107Configuration;

import io.github.jhipster.config.jcache.BeanClassLoaderAwareJCacheRegionFactory;
import io.github.jhipster.config.JHipsterProperties;

import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        BeanClassLoaderAwareJCacheRegionFactory.setBeanClassLoader(this.getClass().getClassLoader());
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcache.getTimeToLiveSeconds())))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            cm.createCache(ecom.myresto.repository.UserRepository.USERS_BY_LOGIN_CACHE, jcacheConfiguration);
            cm.createCache(ecom.myresto.repository.UserRepository.USERS_BY_EMAIL_CACHE, jcacheConfiguration);
            cm.createCache(ecom.myresto.domain.User.class.getName(), jcacheConfiguration);
            cm.createCache(ecom.myresto.domain.Authority.class.getName(), jcacheConfiguration);
            cm.createCache(ecom.myresto.domain.User.class.getName() + ".authorities", jcacheConfiguration);
            cm.createCache(ecom.myresto.domain.Restaurant.class.getName(), jcacheConfiguration);
            cm.createCache(ecom.myresto.domain.Restaurant.class.getName() + ".services", jcacheConfiguration);
            cm.createCache(ecom.myresto.domain.Restaurant.class.getName() + ".gestionnaires", jcacheConfiguration);
            cm.createCache(ecom.myresto.domain.Carte.class.getName(), jcacheConfiguration);
            cm.createCache(ecom.myresto.domain.Carte.class.getName() + ".menus", jcacheConfiguration);
            cm.createCache(ecom.myresto.domain.Carte.class.getName() + ".plats", jcacheConfiguration);
            cm.createCache(ecom.myresto.domain.Plat.class.getName(), jcacheConfiguration);
            cm.createCache(ecom.myresto.domain.Plat.class.getName() + ".menus", jcacheConfiguration);
            cm.createCache(ecom.myresto.domain.Menu.class.getName(), jcacheConfiguration);
            cm.createCache(ecom.myresto.domain.Menu.class.getName() + ".plats", jcacheConfiguration);
            cm.createCache(ecom.myresto.domain.Services.class.getName(), jcacheConfiguration);
            cm.createCache(ecom.myresto.domain.Services.class.getName() + ".reservations", jcacheConfiguration);
            cm.createCache(ecom.myresto.domain.Reservation.class.getName(), jcacheConfiguration);
            cm.createCache(ecom.myresto.domain.Reservation.class.getName() + ".tables", jcacheConfiguration);
            cm.createCache(ecom.myresto.domain.Tables.class.getName(), jcacheConfiguration);
            cm.createCache(ecom.myresto.domain.Client.class.getName(), jcacheConfiguration);
            cm.createCache(ecom.myresto.domain.Client.class.getName() + ".reservations", jcacheConfiguration);
            cm.createCache(ecom.myresto.domain.Serveur.class.getName(), jcacheConfiguration);
            cm.createCache(ecom.myresto.domain.Serveur.class.getName() + ".reservations", jcacheConfiguration);
            cm.createCache(ecom.myresto.domain.Gestionnaire.class.getName(), jcacheConfiguration);
            cm.createCache(ecom.myresto.domain.Gestionnaire.class.getName() + ".supervises", jcacheConfiguration);
            cm.createCache(ecom.myresto.domain.Gestionnaire.class.getName() + ".restaurants", jcacheConfiguration);
            cm.createCache(ecom.myresto.domain.Restaurant.class.getName() + ".ouvertures", jcacheConfiguration);
            cm.createCache(ecom.myresto.domain.Restaurant.class.getName() + ".types", jcacheConfiguration);
            cm.createCache(ecom.myresto.domain.Ouverture.class.getName(), jcacheConfiguration);
            cm.createCache(ecom.myresto.domain.Type.class.getName(), jcacheConfiguration);
            cm.createCache(ecom.myresto.domain.Tables.class.getName() + ".reservations", jcacheConfiguration);
            // jhipster-needle-ehcache-add-entry
        };
    }
}
