package ecom.myresto.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Restaurant.
 */
@Entity
@Table(name = "restaurant")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Restaurant implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "nom", nullable = false)
    private String nom;

    @NotNull
    @Column(name = "adresse", nullable = false)
    private String adresse;

    @NotNull
    @Min(value = 1)
    @Column(name = "capacite", nullable = false)
    private Integer capacite;

    @NotNull
    @Column(name = "latitude", nullable = false)
    private Float latitude;

    @NotNull
    @Column(name = "longitude", nullable = false)
    private Float longitude;

    @NotNull
    @Min(value = 1)
    @Column(name = "nbtables", nullable = false)
    private Integer nbtables;

    @OneToOne    @JoinColumn(unique = true)
    private Carte carte;

    @OneToMany(mappedBy = "restaurant")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Ouverture> ouvertures = new HashSet<>();
    @OneToMany(mappedBy = "restaurant")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Type> types = new HashSet<>();
    @OneToOne(mappedBy = "restaurant")
    @JsonIgnore
    private Serveur serveur;

    @OneToMany(mappedBy = "restaurant")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Services> services = new HashSet<>();
    @ManyToMany(mappedBy = "restaurants")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JsonIgnore
    private Set<Gestionnaire> gestionnaires = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public Restaurant nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAdresse() {
        return adresse;
    }

    public Restaurant adresse(String adresse) {
        this.adresse = adresse;
        return this;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public Integer getCapacite() {
        return capacite;
    }

    public Restaurant capacite(Integer capacite) {
        this.capacite = capacite;
        return this;
    }

    public void setCapacite(Integer capacite) {
        this.capacite = capacite;
    }

    public Float getLatitude() {
        return latitude;
    }

    public Restaurant latitude(Float latitude) {
        this.latitude = latitude;
        return this;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public Restaurant longitude(Float longitude) {
        this.longitude = longitude;
        return this;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public Integer getNbtables() {
        return nbtables;
    }

    public Restaurant nbtables(Integer nbtables) {
        this.nbtables = nbtables;
        return this;
    }

    public void setNbtables(Integer nbtables) {
        this.nbtables = nbtables;
    }

    public Carte getCarte() {
        return carte;
    }

    public Restaurant carte(Carte carte) {
        this.carte = carte;
        return this;
    }

    public void setCarte(Carte carte) {
        this.carte = carte;
    }

    public Set<Ouverture> getOuvertures() {
        return ouvertures;
    }

    public Restaurant ouvertures(Set<Ouverture> ouvertures) {
        this.ouvertures = ouvertures;
        return this;
    }

    public Restaurant addOuverture(Ouverture ouverture) {
        this.ouvertures.add(ouverture);
        ouverture.setRestaurant(this);
        return this;
    }

    public Restaurant removeOuverture(Ouverture ouverture) {
        this.ouvertures.remove(ouverture);
        ouverture.setRestaurant(null);
        return this;
    }

    public void setOuvertures(Set<Ouverture> ouvertures) {
        this.ouvertures = ouvertures;
    }

    public Set<Type> getTypes() {
        return types;
    }

    public Restaurant types(Set<Type> types) {
        this.types = types;
        return this;
    }

    public Restaurant addType(Type type) {
        this.types.add(type);
        type.setRestaurant(this);
        return this;
    }

    public Restaurant removeType(Type type) {
        this.types.remove(type);
        type.setRestaurant(null);
        return this;
    }

    public void setTypes(Set<Type> types) {
        this.types = types;
    }

    public Serveur getServeur() {
        return serveur;
    }

    public Restaurant serveur(Serveur serveur) {
        this.serveur = serveur;
        return this;
    }

    public void setServeur(Serveur serveur) {
        this.serveur = serveur;
    }

    public Set<Services> getServices() {
        return services;
    }

    public Restaurant services(Set<Services> services) {
        this.services = services;
        return this;
    }

    public Restaurant addServices(Services services) {
        this.services.add(services);
        services.setRestaurant(this);
        return this;
    }

    public Restaurant removeServices(Services services) {
        this.services.remove(services);
        services.setRestaurant(null);
        return this;
    }

    public void setServices(Set<Services> services) {
        this.services = services;
    }

    public Set<Gestionnaire> getGestionnaires() {
        return gestionnaires;
    }

    public Restaurant gestionnaires(Set<Gestionnaire> gestionnaires) {
        this.gestionnaires = gestionnaires;
        return this;
    }

    public Restaurant addGestionnaire(Gestionnaire gestionnaire) {
        this.gestionnaires.add(gestionnaire);
        gestionnaire.getRestaurants().add(this);
        return this;
    }

    public Restaurant removeGestionnaire(Gestionnaire gestionnaire) {
        this.gestionnaires.remove(gestionnaire);
        gestionnaire.getRestaurants().remove(this);
        return this;
    }

    public void setGestionnaires(Set<Gestionnaire> gestionnaires) {
        this.gestionnaires = gestionnaires;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Restaurant restaurant = (Restaurant) o;
        if (restaurant.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), restaurant.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Restaurant{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            ", adresse='" + getAdresse() + "'" +
            ", capacite=" + getCapacite() +
            ", latitude=" + getLatitude() +
            ", longitude=" + getLongitude() +
            ", nbtables=" + getNbtables() +
            "}";
    }
}
