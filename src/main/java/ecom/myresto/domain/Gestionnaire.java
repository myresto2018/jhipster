package ecom.myresto.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Gestionnaire.
 */
@Entity
@Table(name = "gestionnaire")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Gestionnaire implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @OneToMany(mappedBy = "superviseur")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Gestionnaire> supervises = new HashSet<>();
    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "gestionnaire_restaurant",
               joinColumns = @JoinColumn(name = "gestionnaires_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "restaurants_id", referencedColumnName = "id"))
    private Set<Restaurant> restaurants = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("supervises")
    private Gestionnaire superviseur;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<Gestionnaire> getSupervises() {
        return supervises;
    }

    public Gestionnaire supervises(Set<Gestionnaire> gestionnaires) {
        this.supervises = gestionnaires;
        return this;
    }

    public Gestionnaire addSupervise(Gestionnaire gestionnaire) {
        this.supervises.add(gestionnaire);
        gestionnaire.setSuperviseur(this);
        return this;
    }

    public Gestionnaire removeSupervise(Gestionnaire gestionnaire) {
        this.supervises.remove(gestionnaire);
        gestionnaire.setSuperviseur(null);
        return this;
    }

    public void setSupervises(Set<Gestionnaire> gestionnaires) {
        this.supervises = gestionnaires;
    }

    public Set<Restaurant> getRestaurants() {
        return restaurants;
    }

    public Gestionnaire restaurants(Set<Restaurant> restaurants) {
        this.restaurants = restaurants;
        return this;
    }

    public Gestionnaire addRestaurant(Restaurant restaurant) {
        this.restaurants.add(restaurant);
        restaurant.getGestionnaires().add(this);
        return this;
    }

    public Gestionnaire removeRestaurant(Restaurant restaurant) {
        this.restaurants.remove(restaurant);
        restaurant.getGestionnaires().remove(this);
        return this;
    }

    public void setRestaurants(Set<Restaurant> restaurants) {
        this.restaurants = restaurants;
    }

    public Gestionnaire getSuperviseur() {
        return superviseur;
    }

    public Gestionnaire superviseur(Gestionnaire gestionnaire) {
        this.superviseur = gestionnaire;
        return this;
    }

    public void setSuperviseur(Gestionnaire gestionnaire) {
        this.superviseur = gestionnaire;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Gestionnaire gestionnaire = (Gestionnaire) o;
        if (gestionnaire.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), gestionnaire.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Gestionnaire{" +
            "id=" + getId() +
            "}";
    }
}
