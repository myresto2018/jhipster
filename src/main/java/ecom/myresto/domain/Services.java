package ecom.myresto.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import ecom.myresto.domain.enumeration.HoraireService;

/**
 * A Services.
 */
@Entity
@Table(name = "services")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Services implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "jhi_date", nullable = false)
    private LocalDate date;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "horaire", nullable = false)
    private HoraireService horaire;

    @ManyToOne
    @JsonIgnoreProperties("services")
    private Restaurant restaurant;

    @OneToMany(mappedBy = "services")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Reservation> reservations = new HashSet<>();
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public Services date(LocalDate date) {
        this.date = date;
        return this;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public HoraireService getHoraire() {
        return horaire;
    }

    public Services horaire(HoraireService horaire) {
        this.horaire = horaire;
        return this;
    }

    public void setHoraire(HoraireService horaire) {
        this.horaire = horaire;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public Services restaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
        return this;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public Set<Reservation> getReservations() {
        return reservations;
    }

    public Services reservations(Set<Reservation> reservations) {
        this.reservations = reservations;
        return this;
    }

    public Services addReservation(Reservation reservation) {
        this.reservations.add(reservation);
        reservation.setServices(this);
        return this;
    }

    public Services removeReservation(Reservation reservation) {
        this.reservations.remove(reservation);
        reservation.setServices(null);
        return this;
    }

    public void setReservations(Set<Reservation> reservations) {
        this.reservations = reservations;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Services services = (Services) o;
        if (services.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), services.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Services{" +
            "id=" + getId() +
            ", date='" + getDate() + "'" +
            ", horaire='" + getHoraire() + "'" +
            "}";
    }
}
