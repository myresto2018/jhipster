package ecom.myresto.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Reservation.
 */
@Entity
@Table(name = "reservation")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Reservation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Min(value = 1)
    @Column(name = "nb_couverts", nullable = false)
    private Integer nbCouverts;

    @ManyToOne
    @JsonIgnoreProperties("reservations")
    private Services services;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "reservation_tables",
               joinColumns = @JoinColumn(name = "reservations_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "tables_id", referencedColumnName = "id"))
    private Set<Tables> tables = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("reservations")
    private Serveur serveur;

    @ManyToOne
    @JsonIgnoreProperties("reservations")
    private Client client;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNbCouverts() {
        return nbCouverts;
    }

    public Reservation nbCouverts(Integer nbCouverts) {
        this.nbCouverts = nbCouverts;
        return this;
    }

    public void setNbCouverts(Integer nbCouverts) {
        this.nbCouverts = nbCouverts;
    }

    public Services getServices() {
        return services;
    }

    public Reservation services(Services services) {
        this.services = services;
        return this;
    }

    public void setServices(Services services) {
        this.services = services;
    }

    public Set<Tables> getTables() {
        return tables;
    }

    public Reservation tables(Set<Tables> tables) {
        this.tables = tables;
        return this;
    }

    public Reservation addTables(Tables tables) {
        this.tables.add(tables);
        tables.getReservations().add(this);
        return this;
    }

    public Reservation removeTables(Tables tables) {
        this.tables.remove(tables);
        tables.getReservations().remove(this);
        return this;
    }

    public void setTables(Set<Tables> tables) {
        this.tables = tables;
    }

    public Serveur getServeur() {
        return serveur;
    }

    public Reservation serveur(Serveur serveur) {
        this.serveur = serveur;
        return this;
    }

    public void setServeur(Serveur serveur) {
        this.serveur = serveur;
    }

    public Client getClient() {
        return client;
    }

    public Reservation client(Client client) {
        this.client = client;
        return this;
    }

    public void setClient(Client client) {
        this.client = client;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Reservation reservation = (Reservation) o;
        if (reservation.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), reservation.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Reservation{" +
            "id=" + getId() +
            ", nbCouverts=" + getNbCouverts() +
            "}";
    }
}
