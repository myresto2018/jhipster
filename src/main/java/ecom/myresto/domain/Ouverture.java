package ecom.myresto.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

import ecom.myresto.domain.enumeration.Jour;

import ecom.myresto.domain.enumeration.HoraireService;

/**
 * A Ouverture.
 */
@Entity
@Table(name = "ouverture")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Ouverture implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "day", nullable = false)
    private Jour day;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "creneau", nullable = false)
    private HoraireService creneau;

    @ManyToOne
    @JsonIgnoreProperties("ouvertures")
    private Restaurant restaurant;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Jour getDay() {
        return day;
    }

    public Ouverture day(Jour day) {
        this.day = day;
        return this;
    }

    public void setDay(Jour day) {
        this.day = day;
    }

    public HoraireService getCreneau() {
        return creneau;
    }

    public Ouverture creneau(HoraireService creneau) {
        this.creneau = creneau;
        return this;
    }

    public void setCreneau(HoraireService creneau) {
        this.creneau = creneau;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public Ouverture restaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
        return this;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Ouverture ouverture = (Ouverture) o;
        if (ouverture.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), ouverture.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Ouverture{" +
            "id=" + getId() +
            ", day='" + getDay() + "'" +
            ", creneau='" + getCreneau() + "'" +
            "}";
    }
}
