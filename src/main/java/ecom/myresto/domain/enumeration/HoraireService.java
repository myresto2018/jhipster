package ecom.myresto.domain.enumeration;

/**
 * The HoraireService enumeration.
 */
public enum HoraireService {
    MATIN, MIDI, SOIR
}
