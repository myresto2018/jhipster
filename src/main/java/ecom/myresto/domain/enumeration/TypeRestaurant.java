package ecom.myresto.domain.enumeration;

/**
 * The TypeRestaurant enumeration.
 */
public enum TypeRestaurant {
    FASTFOOD, PIZZERIA, BURGER, GASTRONOMIQUE, INDIEN, CHINOIS, TRADITIONNEL, SAVOYARD
}
