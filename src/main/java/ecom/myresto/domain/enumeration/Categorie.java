package ecom.myresto.domain.enumeration;

/**
 * The Categorie enumeration.
 */
public enum Categorie {
    PLAT, BOISSON, DESSERT, ENTREE
}
