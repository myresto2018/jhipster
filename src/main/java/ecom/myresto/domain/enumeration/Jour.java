package ecom.myresto.domain.enumeration;

/**
 * The Jour enumeration.
 */
public enum Jour {
    LUNDI, MARDI, MERCREDI, JEUDI, VENDREDI, SAMEDI, DIMANCHE
}
