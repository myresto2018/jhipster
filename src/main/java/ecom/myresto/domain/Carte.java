package ecom.myresto.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Carte.
 */
@Entity
@Table(name = "carte")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Carte implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @OneToMany(mappedBy = "carte")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Menu> menus = new HashSet<>();
    @OneToMany(mappedBy = "carte")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Plat> plats = new HashSet<>();
    @OneToOne(mappedBy = "carte")
    @JsonIgnore
    private Restaurant restaurant;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<Menu> getMenus() {
        return menus;
    }

    public Carte menus(Set<Menu> menus) {
        this.menus = menus;
        return this;
    }

    public Carte addMenu(Menu menu) {
        this.menus.add(menu);
        menu.setCarte(this);
        return this;
    }

    public Carte removeMenu(Menu menu) {
        this.menus.remove(menu);
        menu.setCarte(null);
        return this;
    }

    public void setMenus(Set<Menu> menus) {
        this.menus = menus;
    }

    public Set<Plat> getPlats() {
        return plats;
    }

    public Carte plats(Set<Plat> plats) {
        this.plats = plats;
        return this;
    }

    public Carte addPlat(Plat plat) {
        this.plats.add(plat);
        plat.setCarte(this);
        return this;
    }

    public Carte removePlat(Plat plat) {
        this.plats.remove(plat);
        plat.setCarte(null);
        return this;
    }

    public void setPlats(Set<Plat> plats) {
        this.plats = plats;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public Carte restaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
        return this;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Carte carte = (Carte) o;
        if (carte.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), carte.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Carte{" +
            "id=" + getId() +
            "}";
    }
}
