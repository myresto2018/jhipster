package ecom.myresto.web.rest;

import com.codahale.metrics.annotation.Timed;
import ecom.myresto.domain.Tables;
import ecom.myresto.repository.TablesRepository;
import ecom.myresto.web.rest.errors.BadRequestAlertException;
import ecom.myresto.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Tables.
 */
@RestController
@RequestMapping("/api")
public class TablesResource {

    private final Logger log = LoggerFactory.getLogger(TablesResource.class);

    private static final String ENTITY_NAME = "tables";

    private final TablesRepository tablesRepository;

    public TablesResource(TablesRepository tablesRepository) {
        this.tablesRepository = tablesRepository;
    }

    /**
     * POST  /tables : Create a new tables.
     *
     * @param tables the tables to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tables, or with status 400 (Bad Request) if the tables has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/tables")
    @Timed
    public ResponseEntity<Tables> createTables(@Valid @RequestBody Tables tables) throws URISyntaxException {
        log.debug("REST request to save Tables : {}", tables);
        if (tables.getId() != null) {
            throw new BadRequestAlertException("A new tables cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Tables result = tablesRepository.save(tables);
        return ResponseEntity.created(new URI("/api/tables/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /tables : Updates an existing tables.
     *
     * @param tables the tables to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tables,
     * or with status 400 (Bad Request) if the tables is not valid,
     * or with status 500 (Internal Server Error) if the tables couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/tables")
    @Timed
    public ResponseEntity<Tables> updateTables(@Valid @RequestBody Tables tables) throws URISyntaxException {
        log.debug("REST request to update Tables : {}", tables);
        if (tables.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Tables result = tablesRepository.save(tables);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, tables.getId().toString()))
            .body(result);
    }

    /**
     * GET  /tables : get all the tables.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of tables in body
     */
    @GetMapping("/tables")
    @Timed
    public List<Tables> getAllTables() {
        log.debug("REST request to get all Tables");
        return tablesRepository.findAll();
    }

    /**
     * GET  /tables/:id : get the "id" tables.
     *
     * @param id the id of the tables to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tables, or with status 404 (Not Found)
     */
    @GetMapping("/tables/{id}")
    @Timed
    public ResponseEntity<Tables> getTables(@PathVariable Long id) {
        log.debug("REST request to get Tables : {}", id);
        Optional<Tables> tables = tablesRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(tables);
    }

    /**
     * DELETE  /tables/:id : delete the "id" tables.
     *
     * @param id the id of the tables to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/tables/{id}")
    @Timed
    public ResponseEntity<Void> deleteTables(@PathVariable Long id) {
        log.debug("REST request to delete Tables : {}", id);

        tablesRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
