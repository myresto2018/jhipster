/**
 * View Models used by Spring MVC REST controllers.
 */
package ecom.myresto.web.rest.vm;
