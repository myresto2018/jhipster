package ecom.myresto.web.rest;

import com.codahale.metrics.annotation.Timed;
import ecom.myresto.domain.Restaurant;
import ecom.myresto.domain.enumeration.HoraireService;
import ecom.myresto.domain.enumeration.Jour;
import ecom.myresto.domain.enumeration.TypeRestaurant;
import ecom.myresto.repository.RestaurantRepository;
import ecom.myresto.web.rest.errors.BadRequestAlertException;
import ecom.myresto.web.rest.util.HeaderUtil;
import ecom.myresto.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * REST controller for managing Restaurant.
 */
@RestController
@RequestMapping("/api")
public class RestaurantResource {

	private final Logger log = LoggerFactory.getLogger(RestaurantResource.class);

	private static final String ENTITY_NAME = "restaurant";

	private final RestaurantRepository restaurantRepository;

	public RestaurantResource(RestaurantRepository restaurantRepository) {
		this.restaurantRepository = restaurantRepository;
	}

	/**
	 * POST /restaurants : Create a new restaurant.
	 *
	 * @param restaurant the restaurant to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         restaurant, or with status 400 (Bad Request) if the restaurant has
	 *         already an ID
	 * @throws URISyntaxException if the Location URI syntax is incorrect
	 */
	@PostMapping("/restaurants")
	@Timed
	public ResponseEntity<Restaurant> createRestaurant(@Valid @RequestBody Restaurant restaurant)
			throws URISyntaxException {
		log.debug("REST request to save Restaurant : {}", restaurant);
		if (restaurant.getId() != null) {
			throw new BadRequestAlertException("A new restaurant cannot already have an ID", ENTITY_NAME, "idexists");
		}
		Restaurant result = restaurantRepository.save(restaurant);
		return ResponseEntity.created(new URI("/api/restaurants/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /restaurants : Updates an existing restaurant.
	 *
	 * @param restaurant the restaurant to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         restaurant, or with status 400 (Bad Request) if the restaurant is not
	 *         valid, or with status 500 (Internal Server Error) if the restaurant
	 *         couldn't be updated
	 * @throws URISyntaxException if the Location URI syntax is incorrect
	 */
	@PutMapping("/restaurants")
	@Timed
	public ResponseEntity<Restaurant> updateRestaurant(@Valid @RequestBody Restaurant restaurant)
			throws URISyntaxException {
		log.debug("REST request to update Restaurant : {}", restaurant);
		if (restaurant.getId() == null) {
			throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
		}
		Restaurant result = restaurantRepository.save(restaurant);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, restaurant.getId().toString())).body(result);
	}

	/**
	 * GET /restaurants : get all the restaurants.
	 *
	 * @param pageable the pagination information
	 * @param filter   the filter of the request
	 * @return the ResponseEntity with status 200 (OK) and the list of restaurants
	 *         in body
	 */
	@GetMapping("/restaurants")
	@Timed
	public ResponseEntity<List<Restaurant>> getAllRestaurants(Pageable pageable,
			@RequestParam(required = false) String filter) {
		if ("serveur-is-null".equals(filter)) {
			log.debug("REST request to get all Restaurants where serveur is null");
			return new ResponseEntity<>(
					StreamSupport.stream(restaurantRepository.findAll().spliterator(), false)
							.filter(restaurant -> restaurant.getServeur() == null).collect(Collectors.toList()),
					HttpStatus.OK);
		}
		log.debug("REST request to get a page of Restaurants");
		Page<Restaurant> page = restaurantRepository.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/restaurants");
		return ResponseEntity.ok().headers(headers).body(page.getContent());
	}

	/**
	 * GET /restaurants/:id : get the "id" restaurant.
	 *
	 * @param id the id of the restaurant to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the restaurant,
	 *         or with status 404 (Not Found)
	 */
	@GetMapping("/restaurants/{id}")
	@Timed
	public ResponseEntity<Restaurant> getRestaurant(@PathVariable Long id) {
		log.debug("REST request to get Restaurant : {}", id);
		Optional<Restaurant> restaurant = restaurantRepository.findById(id);
		return ResponseUtil.wrapOrNotFound(restaurant);
	}

	/**
	 * DELETE /restaurants/:id : delete the "id" restaurant.
	 *
	 * @param id the id of the restaurant to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/restaurants/{id}")
	@Timed
	public ResponseEntity<Void> deleteRestaurant(@PathVariable Long id) {
		log.debug("REST request to delete Restaurant : {}", id);

		restaurantRepository.deleteById(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

	/**
	 * GET /restaurants/:id/statistic/pdf : get pdf resume of the "id" restaurant.
	 *
	 * @param id the id of the restaurant to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the PDF, or
	 *         with status 404 (Not Found)
	 */
	@GetMapping("/restaurants/{id}/statistic/pdf")
	@Timed
	public ResponseEntity<InputStreamResource> getStatisticPdf(@PathVariable Long id) throws IOException {
		log.debug("REST request to get Restaurant PDF : {}", id);
		Optional<Restaurant> restaurant = restaurantRepository.findById(id);

		ByteArrayOutputStream output = new ByteArrayOutputStream();

		PDDocument document = new PDDocument();
		PDPage page = new PDPage();
		document.addPage(page);

		PDFont font = PDType1Font.HELVETICA;
		float fontSize = 16;
		float leading = 1.5f * fontSize;

		PDRectangle mediabox = page.getMediaBox();
		float margin = 64;
		float startX = mediabox.getLowerLeftX() + margin;
		float startY = mediabox.getUpperRightY() - margin;

		PDPageContentStream contentStream = new PDPageContentStream(document, page);
		contentStream.beginText();
		contentStream.setFont(font, fontSize);
		contentStream.setLeading(leading);
		contentStream.newLineAtOffset(startX, startY);
		contentStream.showText("Restaurant: " + restaurant.get().getNom());
		contentStream.newLine();
		contentStream.showText("Adresse: " + restaurant.get().getAdresse());
		contentStream.newLine();
		contentStream.showText("Capacité: " + restaurant.get().getCapacite());
		contentStream.endText();
		contentStream.close();
		document.save(output);

		ByteArrayInputStream bis = new ByteArrayInputStream(output.toByteArray());

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "inline; filename=report.pdf");

		return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF)
				.body(new InputStreamResource(bis));
	}

	/**
	 * GET /restaurants/:id/statistic : get the statistic for the "id" restaurant.
	 *
	 * @param id the id of the restaurant to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the restaurant,
	 *         or with status 404 (Not Found)
	 */
	// TODO add filter on matin / midi / soir
	@GetMapping("/restaurants/{id}/statistic")
	@Timed
	public ResponseEntity<Double> getRestaurantStatistic(@PathVariable Long id, @RequestParam LocalDate deb,
			@RequestParam LocalDate fin) {
		log.debug("REST request to get statistic of the restaurant : {}", id);

		if (id == null) {
			return new ResponseEntity<Double>(0.0, HttpStatus.EXPECTATION_FAILED);
		}

		ArrayList<Integer> listing;
		ArrayList<Long> list_service;

		if (deb == null && fin == null) {
			listing = restaurantRepository.retrieveStatistic(id);
			list_service = restaurantRepository.retrieveService(id);
		} else if (deb == null) {
			listing = restaurantRepository.retrieveStatisticFin(id, fin);
			list_service = restaurantRepository.retrieveServiceFin(id, fin);
		} else if (fin == null) {
			listing = restaurantRepository.retrieveStatisticDeb(id, deb);
			list_service = restaurantRepository.retrieveServiceFin(id, fin);
		} else {
			listing = restaurantRepository.retrieveStatisticDebFin(id, deb, fin);
			list_service = restaurantRepository.retrieveServiceDebFin(id, deb, fin);
		}

		double sum = 0;
		for (Integer d : listing)
			sum += d;

		double i = (double) restaurantRepository.retrieveTaux(id);
		sum = sum / (list_service.size() * i);

		// Double taux = new Double(restaurantRepository.retrieveTaux(id));
		// sum = sum / (listing.size() * taux);
		return new ResponseEntity<Double>(new Double(sum), HttpStatus.OK);
		// return ResponseUtil.wrapOrNotFound(restaurant);
	}

	/**
	 * GET /restaurants/getRestaurantFilter : get the statistic for the "id"
	 * restaurant.
	 *
	 * @param id the id of the restaurant to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the restaurant,
	 *         or with status 404 (Not Found)
	 */
	// dates de debut et de fin -- capactie -- gastronomie
	@GetMapping("/restaurants/getRestaurantFilter")
	@Timed
	public ResponseEntity<List<Restaurant>> getRestaurantFilter(@RequestParam Integer fastfood,
			@RequestParam Integer burger, @RequestParam Integer pizzeria, @RequestParam Integer gastronomique,
			@RequestParam Integer indien, @RequestParam Integer chinois, @RequestParam Integer traditionnel,
			@RequestParam Integer savoyard, @RequestParam Integer lundi, @RequestParam Integer mardi,
			@RequestParam Integer mercredi, @RequestParam Integer jeudi, @RequestParam Integer vendredi,
			@RequestParam Integer samedi, @RequestParam Integer dimanche, @RequestParam Integer matin,
			@RequestParam Integer midi, @RequestParam Integer soir) {
		log.debug("REST request to get statistic of the restaurant : {}");

		ArrayList<TypeRestaurant> tr = new ArrayList<TypeRestaurant>();
		
		if (fastfood == 1)
			tr.add(TypeRestaurant.FASTFOOD);
		if (burger == 1)
			tr.add(TypeRestaurant.BURGER);
		if (pizzeria == 1)
			tr.add(TypeRestaurant.PIZZERIA);
		if (gastronomique == 1)
			tr.add(TypeRestaurant.GASTRONOMIQUE);
		if (indien == 1)
			tr.add(TypeRestaurant.INDIEN);
		if (chinois == 1)
			tr.add(TypeRestaurant.CHINOIS);
		if (traditionnel == 1)
			tr.add(TypeRestaurant.TRADITIONNEL);
		if (savoyard == 1)
			tr.add(TypeRestaurant.SAVOYARD);

		ArrayList<Jour> days = new ArrayList<Jour>();
		ArrayList<HoraireService> creneaux = new ArrayList<HoraireService>();
		
		if (lundi == 1)
			days.add(Jour.LUNDI);
		if (mardi == 1)
			days.add(Jour.MARDI);
		if (mercredi == 1)
			days.add(Jour.MERCREDI);
		if (jeudi == 1)
			days.add(Jour.JEUDI);
		if (vendredi == 1)
			days.add(Jour.VENDREDI);
		if (samedi == 1)
			days.add(Jour.SAMEDI);
		if (dimanche == 1)
			days.add(Jour.DIMANCHE);
		if (matin == 1)
			creneaux.add(HoraireService.MATIN);
		if (midi == 1)
			creneaux.add(HoraireService.MIDI);
		if (soir == 1)
			creneaux.add(HoraireService.SOIR);
		
		ArrayList<Restaurant> lr1 = restaurantRepository.getRestaurantFilterType(tr);
		ArrayList<Restaurant> lr2 = restaurantRepository.getRestaurantFilterOpening(days, creneaux);
		
		ArrayList<Restaurant> lr = restaurantRepository.getRestaurantFilter(lr1, lr2);
		return new ResponseEntity<List<Restaurant>>(lr, HttpStatus.OK);
	}
}
