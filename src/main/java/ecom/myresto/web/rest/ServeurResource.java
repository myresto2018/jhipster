package ecom.myresto.web.rest;

import com.codahale.metrics.annotation.Timed;
import ecom.myresto.domain.Serveur;
import ecom.myresto.repository.ServeurRepository;
import ecom.myresto.web.rest.errors.BadRequestAlertException;
import ecom.myresto.web.rest.util.HeaderUtil;
import ecom.myresto.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Serveur.
 */
@RestController
@RequestMapping("/api")
public class ServeurResource {

    private final Logger log = LoggerFactory.getLogger(ServeurResource.class);

    private static final String ENTITY_NAME = "serveur";

    private ServeurRepository serveurRepository;

    public ServeurResource(ServeurRepository serveurRepository) {
        this.serveurRepository = serveurRepository;
    }

    /**
     * POST  /serveurs : Create a new serveur.
     *
     * @param serveur the serveur to create
     * @return the ResponseEntity with status 201 (Created) and with body the new serveur, or with status 400 (Bad Request) if the serveur has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/serveurs")
    @Timed
    public ResponseEntity<Serveur> createServeur(@RequestBody Serveur serveur) throws URISyntaxException {
        log.debug("REST request to save Serveur : {}", serveur);
        if (serveur.getId() != null) {
            throw new BadRequestAlertException("A new serveur cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Serveur result = serveurRepository.save(serveur);
        return ResponseEntity.created(new URI("/api/serveurs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /serveurs : Updates an existing serveur.
     *
     * @param serveur the serveur to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated serveur,
     * or with status 400 (Bad Request) if the serveur is not valid,
     * or with status 500 (Internal Server Error) if the serveur couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/serveurs")
    @Timed
    public ResponseEntity<Serveur> updateServeur(@RequestBody Serveur serveur) throws URISyntaxException {
        log.debug("REST request to update Serveur : {}", serveur);
        if (serveur.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Serveur result = serveurRepository.save(serveur);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, serveur.getId().toString()))
            .body(result);
    }

    /**
     * GET  /serveurs : get all the serveurs.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of serveurs in body
     */
    @GetMapping("/serveurs")
    @Timed
    public ResponseEntity<List<Serveur>> getAllServeurs(Pageable pageable) {
        log.debug("REST request to get a page of Serveurs");
        Page<Serveur> page = serveurRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/serveurs");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /serveurs/:id : get the "id" serveur.
     *
     * @param id the id of the serveur to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the serveur, or with status 404 (Not Found)
     */
    @GetMapping("/serveurs/{id}")
    @Timed
    public ResponseEntity<Serveur> getServeur(@PathVariable Long id) {
        log.debug("REST request to get Serveur : {}", id);
        Optional<Serveur> serveur = serveurRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(serveur);
    }

    /**
     * DELETE  /serveurs/:id : delete the "id" serveur.
     *
     * @param id the id of the serveur to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/serveurs/{id}")
    @Timed
    public ResponseEntity<Void> deleteServeur(@PathVariable Long id) {
        log.debug("REST request to delete Serveur : {}", id);

        serveurRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
