package ecom.myresto.web.rest;

import com.codahale.metrics.annotation.Timed;
import ecom.myresto.domain.Ouverture;
import ecom.myresto.repository.OuvertureRepository;
import ecom.myresto.web.rest.errors.BadRequestAlertException;
import ecom.myresto.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Ouverture.
 */
@RestController
@RequestMapping("/api")
public class OuvertureResource {

    private final Logger log = LoggerFactory.getLogger(OuvertureResource.class);

    private static final String ENTITY_NAME = "ouverture";

    private final OuvertureRepository ouvertureRepository;

    public OuvertureResource(OuvertureRepository ouvertureRepository) {
        this.ouvertureRepository = ouvertureRepository;
    }

    /**
     * POST  /ouvertures : Create a new ouverture.
     *
     * @param ouverture the ouverture to create
     * @return the ResponseEntity with status 201 (Created) and with body the new ouverture, or with status 400 (Bad Request) if the ouverture has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/ouvertures")
    @Timed
    public ResponseEntity<Ouverture> createOuverture(@Valid @RequestBody Ouverture ouverture) throws URISyntaxException {
        log.debug("REST request to save Ouverture : {}", ouverture);
        if (ouverture.getId() != null) {
            throw new BadRequestAlertException("A new ouverture cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Ouverture result = ouvertureRepository.save(ouverture);
        return ResponseEntity.created(new URI("/api/ouvertures/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /ouvertures : Updates an existing ouverture.
     *
     * @param ouverture the ouverture to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated ouverture,
     * or with status 400 (Bad Request) if the ouverture is not valid,
     * or with status 500 (Internal Server Error) if the ouverture couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/ouvertures")
    @Timed
    public ResponseEntity<Ouverture> updateOuverture(@Valid @RequestBody Ouverture ouverture) throws URISyntaxException {
        log.debug("REST request to update Ouverture : {}", ouverture);
        if (ouverture.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Ouverture result = ouvertureRepository.save(ouverture);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, ouverture.getId().toString()))
            .body(result);
    }

    /**
     * GET  /ouvertures : get all the ouvertures.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of ouvertures in body
     */
    @GetMapping("/ouvertures")
    @Timed
    public List<Ouverture> getAllOuvertures() {
        log.debug("REST request to get all Ouvertures");
        return ouvertureRepository.findAll();
    }

    /**
     * GET  /ouvertures/:id : get the "id" ouverture.
     *
     * @param id the id of the ouverture to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the ouverture, or with status 404 (Not Found)
     */
    @GetMapping("/ouvertures/{id}")
    @Timed
    public ResponseEntity<Ouverture> getOuverture(@PathVariable Long id) {
        log.debug("REST request to get Ouverture : {}", id);
        Optional<Ouverture> ouverture = ouvertureRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(ouverture);
    }

    /**
     * DELETE  /ouvertures/:id : delete the "id" ouverture.
     *
     * @param id the id of the ouverture to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/ouvertures/{id}")
    @Timed
    public ResponseEntity<Void> deleteOuverture(@PathVariable Long id) {
        log.debug("REST request to delete Ouverture : {}", id);

        ouvertureRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
