package ecom.myresto.web.rest;

import com.codahale.metrics.annotation.Timed;
import ecom.myresto.domain.Reservation;
import ecom.myresto.repository.ReservationRepository;
import ecom.myresto.web.rest.errors.BadRequestAlertException;
import ecom.myresto.web.rest.util.HeaderUtil;
import ecom.myresto.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Reservation.
 */
@RestController
@RequestMapping("/api")
public class ReservationResource {

    private final Logger log = LoggerFactory.getLogger(ReservationResource.class);

    private static final String ENTITY_NAME = "reservation";

    private final ReservationRepository reservationRepository;

    public ReservationResource(ReservationRepository reservationRepository) {
        this.reservationRepository = reservationRepository;
    }

    /**
     * POST  /reservations : Create a new reservation.
     *
     * @param reservation the reservation to create
     * @return the ResponseEntity with status 201 (Created) and with body the new reservation, or with status 400 (Bad Request) if the reservation has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/reservations")
    @Timed
    public ResponseEntity<Reservation> createReservation(@Valid @RequestBody Reservation reservation) throws URISyntaxException {
        log.debug("REST request to save Reservation : {}", reservation);
        if (reservation.getId() != null) {
            throw new BadRequestAlertException("A new reservation cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Reservation result = reservationRepository.save(reservation);
        return ResponseEntity.created(new URI("/api/reservations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /reservations : Updates an existing reservation.
     *
     * @param reservation the reservation to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated reservation,
     * or with status 400 (Bad Request) if the reservation is not valid,
     * or with status 500 (Internal Server Error) if the reservation couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/reservations")
    @Timed
    public ResponseEntity<Reservation> updateReservation(@Valid @RequestBody Reservation reservation) throws URISyntaxException {
        log.debug("REST request to update Reservation : {}", reservation);
        if (reservation.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Reservation result = reservationRepository.save(reservation);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, reservation.getId().toString()))
            .body(result);
    }

    /**
     * GET  /reservations : get all the reservations.
     *
     * @param pageable the pagination information
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many)
     * @return the ResponseEntity with status 200 (OK) and the list of reservations in body
     */
    @GetMapping("/reservations")
    @Timed
    public ResponseEntity<List<Reservation>> getAllReservations(Pageable pageable, @RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get a page of Reservations");
        Page<Reservation> page;
        if (eagerload) {
            page = reservationRepository.findAllWithEagerRelationships(pageable);
        } else {
            page = reservationRepository.findAll(pageable);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, String.format("/api/reservations?eagerload=%b", eagerload));
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /reservations/:id : get the "id" reservation.
     *
     * @param id the id of the reservation to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the reservation, or with status 404 (Not Found)
     */
    @GetMapping("/reservations/{id}")
    @Timed
    public ResponseEntity<Reservation> getReservation(@PathVariable Long id) {
        log.debug("REST request to get Reservation : {}", id);
        Optional<Reservation> reservation = reservationRepository.findOneWithEagerRelationships(id);
        return ResponseUtil.wrapOrNotFound(reservation);
    }

    /**
     * DELETE  /reservations/:id : delete the "id" reservation.
     *
     * @param id the id of the reservation to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/reservations/{id}")
    @Timed
    public ResponseEntity<Void> deleteReservation(@PathVariable Long id) {
        log.debug("REST request to delete Reservation : {}", id);

        reservationRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * GET /reservations/countResaForRestau/:id
     *
     * @param id The id of the restaurant where the reservation will be retrieved
     * @return The number of reservation with status 200 (OK) or with status 404
     *         (Not Found)
     */
    @GetMapping("/reservations/countResaForRestau/{id}")
    @Timed

    public ResponseEntity<Integer> getResaCount(@PathVariable Long id, @RequestParam LocalDate date) {
        log.debug("REST request to get reservation count of the restaurant : {}", id);
        if(id == null) {
            return new ResponseEntity<Integer>(0, HttpStatus.EXPECTATION_FAILED);
        }

        Integer count = 0;
        count = reservationRepository.retrieveReservationNumber(id, date);
        return new ResponseEntity<Integer>(count, HttpStatus.OK);

    }

    /**
     * GET /reservation/countServiceReservation/:id
     * @param id The id of the service where the reservation will be retrieved
     * @return The number of reservation for the specified service with status 200 (OK), of with status 404(Not Found)
     *
     */
    @GetMapping("/reservations/countResaForService/{id}")
    @Timed

    public ResponseEntity<Integer> getServiceReservations(@PathVariable Long id){
        log.debug("REST request to get reservation count for the service : {}", id);
        if(id==null) {
            return new ResponseEntity<Integer>(0, HttpStatus.EXPECTATION_FAILED);
        }

        Integer count = 0;
        count = reservationRepository.retriveReservationNumberFromService(id);
        return new ResponseEntity<Integer>(count, HttpStatus.OK);

    }

    /**
     * Get /reservation/countCouvertsForReservation/:id
     * @param id The id of the service where the flatware number will be retrieved
     * @return The number of flatware for the specified service with status 200 (OK), of with status 404(Not Found)
     */

    @GetMapping("/reservations/countCouvertsForReservation/{id}")
    @Timed

    public ResponseEntity<Integer> getServiceFlatware(@PathVariable Long id){
        log.debug("REST request to get flatware count for the service : {}", id);
        if(id == null) {
            return new ResponseEntity<Integer>(0, HttpStatus.EXPECTATION_FAILED);
        }

        Integer count = 0;
        count = reservationRepository.retriveReservationNumberFromService(id);
        return new ResponseEntity<Integer>(count, HttpStatus.OK);
    }
}
