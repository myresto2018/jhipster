package ecom.myresto.repository;

import ecom.myresto.domain.Restaurant;
import ecom.myresto.domain.enumeration.HoraireService;
import ecom.myresto.domain.enumeration.Jour;
import ecom.myresto.domain.enumeration.TypeRestaurant;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.ArrayList;



/**
 * Spring Data  repository for the Restaurant entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RestaurantRepository extends JpaRepository<Restaurant, Long> {

    /* SCENARIO 1 RELATED*/
    @Query("SELECT r.nbCouverts FROM Reservation r WHERE LOWER(r.services) IN ("
            + "SELECT LOWER(s.id) FROM Services s WHERE " + "LOWER(s.date) >= LOWER(:deb) "
            + "AND LOWER(s.date) <= LOWER(:fin) " + "AND LOWER(s.restaurant) = LOWER(:id))")
    ArrayList<Integer> retrieveStatisticDebFin(@Param("id") Long restau, @Param("deb") LocalDate debut, @Param("fin") LocalDate ending);

    @Query("SELECT r.nbCouverts FROM Reservation r WHERE LOWER(r.services) IN ("
            + "SELECT LOWER(s.id) FROM Services s WHERE " + "LOWER(s.date) >= LOWER(:deb) "
            + "AND LOWER(s.date) = LOWER(:id))")
    ArrayList<Integer> retrieveStatisticDeb(@Param("id") Long restau, @Param("deb") LocalDate debut);

    @Query("SELECT r.nbCouverts FROM Reservation r WHERE LOWER(r.services) IN ("
            + "SELECT LOWER(s.id) FROM Services s WHERE LOWER(s.date) <= LOWER(:fin) " + "AND LOWER(s.restaurant) = LOWER(:id))")
    ArrayList<Integer> retrieveStatisticFin(@Param("id") Long restau, @Param("fin") LocalDate ending);

    @Query("SELECT r.nbCouverts FROM Reservation r WHERE LOWER(r.services) IN ("
            + "SELECT LOWER(s.id) FROM Services s WHERE LOWER(s.restaurant) = LOWER(:id))")
    ArrayList<Integer> retrieveStatistic(@Param("id") Long restau);

    @Query("SELECT r.capacite FROM Restaurant r WHERE LOWER(r.id) = LOWER(:id)")
    int retrieveTaux(@Param("id") Long restau);

    @Query("SELECT LOWER(s.id) FROM Services s WHERE LOWER(s.date) >= LOWER(:deb) "
            + "AND LOWER(s.date) <= LOWER(:fin) " + "AND LOWER(s.restaurant) = LOWER(:id)")
    ArrayList<Long>retrieveServiceDebFin(@Param("id") Long restau, @Param("deb") LocalDate debut, @Param("fin") LocalDate ending);

    @Query("SELECT LOWER(s.id) FROM Services s WHERE LOWER(s.date) >= LOWER(:deb) "
            + "AND LOWER(s.restaurant) = LOWER(:id)")
    ArrayList<Long> retrieveServiceDeb(@Param("id") Long restau, @Param("deb") LocalDate debut);

    @Query("SELECT LOWER(s.id) FROM Services s WHERE LOWER(s.date) <= LOWER(:fin) " + "AND LOWER(s.restaurant) = LOWER(:id)")
    ArrayList<Long> retrieveServiceFin(@Param("id") Long restau, @Param("fin") LocalDate ending);

    @Query("SELECT LOWER(s.id) FROM Services s WHERE LOWER(s.restaurant) = LOWER(:id)")
    ArrayList<Long> retrieveService(@Param("id") Long restau);

    /* SCENARIO 4 RELATED*/
    @Query("SELECT DISTINCT type.restaurant FROM Type type WHERE type.typeu IN (:typed)")
    ArrayList<Restaurant> getRestaurantFilterType(@Param("typed") ArrayList<TypeRestaurant> s);
    
    @Query("SELECT DISTINCT ouv.restaurant FROM Ouverture ouv WHERE "
    		+ "ouv.day IN (:days) AND ouv.creneau IN (:creneaux)")
    ArrayList<Restaurant> getRestaurantFilterOpening(@Param("days") ArrayList<Jour> days, @Param("creneaux") ArrayList<HoraireService> creneaux);

    @Query("SELECT DISTINCT restaurant FROM Restaurant restaurant WHERE "
    		+ "restaurant IN (:tab1) AND restaurant IN (:tab2)")
    ArrayList<Restaurant> getRestaurantFilter(@Param("tab1") ArrayList<Restaurant> lr1, @Param("tab2") ArrayList<Restaurant> lr2);

}
