package ecom.myresto.repository;

import ecom.myresto.domain.Reservation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Reservation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Long> {

    @Query(value = "select distinct reservation from Reservation reservation left join fetch reservation.tables",
        countQuery = "select count(distinct reservation) from Reservation reservation")
    Page<Reservation> findAllWithEagerRelationships(Pageable pageable);

    @Query(value = "select distinct reservation from Reservation reservation left join fetch reservation.tables")
    List<Reservation> findAllWithEagerRelationships();

    @Query("select reservation from Reservation reservation left join fetch reservation.tables where reservation.id =:id")
    Optional<Reservation> findOneWithEagerRelationships(@Param("id") Long id);

    @Query("SELECT COUNT(r.id) FROM Reservation r WHERE LOWER(r.services) IN("
            + "SELECT LOWER(s.id) FROM Services s WHERE " + "LOWER(s.date) = LOWER(:date) "
            + "AND LOWER(s.restaurant) = LOWER(:id))")
    Integer retrieveReservationNumber(@Param("id") Long restau, @Param("date") LocalDate date);

    @Query("SELECT COUNT(r.id) FROM Reservation r WHERE LOWER(r.services) = LOWER(:serv)")
    Integer retriveReservationNumberFromService(@Param("serv") Long service);

    @Query("SELECT COUNT(r.nbCouverts) FROM Reservation r WHERE LOWER(r.services) = LOWER(:serv)")
    Integer retriveNbCouvertsFromService(@Param("serv") Long service);

}
