package ecom.myresto.repository;

import ecom.myresto.domain.Gestionnaire;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Gestionnaire entity.
 */
@SuppressWarnings("unused")
@Repository
public interface GestionnaireRepository extends JpaRepository<Gestionnaire, Long> {

    @Query(value = "select distinct gestionnaire from Gestionnaire gestionnaire left join fetch gestionnaire.restaurants",
        countQuery = "select count(distinct gestionnaire) from Gestionnaire gestionnaire")
    Page<Gestionnaire> findAllWithEagerRelationships(Pageable pageable);

    @Query(value = "select distinct gestionnaire from Gestionnaire gestionnaire left join fetch gestionnaire.restaurants")
    List<Gestionnaire> findAllWithEagerRelationships();

    @Query("select gestionnaire from Gestionnaire gestionnaire left join fetch gestionnaire.restaurants where gestionnaire.id =:id")
    Optional<Gestionnaire> findOneWithEagerRelationships(@Param("id") Long id);

}
