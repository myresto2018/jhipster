package ecom.myresto.repository;

import ecom.myresto.domain.Ouverture;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Ouverture entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OuvertureRepository extends JpaRepository<Ouverture, Long> {

}
