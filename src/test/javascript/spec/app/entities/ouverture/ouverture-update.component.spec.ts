/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { MyrestoTestModule } from '../../../test.module';
import { OuvertureUpdateComponent } from 'app/entities/ouverture/ouverture-update.component';
import { OuvertureService } from 'app/entities/ouverture/ouverture.service';
import { Ouverture } from 'app/shared/model/ouverture.model';

describe('Component Tests', () => {
    describe('Ouverture Management Update Component', () => {
        let comp: OuvertureUpdateComponent;
        let fixture: ComponentFixture<OuvertureUpdateComponent>;
        let service: OuvertureService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [MyrestoTestModule],
                declarations: [OuvertureUpdateComponent]
            })
                .overrideTemplate(OuvertureUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(OuvertureUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(OuvertureService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Ouverture(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.ouverture = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Ouverture();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.ouverture = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
