/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MyrestoTestModule } from '../../../test.module';
import { OuvertureComponent } from 'app/entities/ouverture/ouverture.component';
import { OuvertureService } from 'app/entities/ouverture/ouverture.service';
import { Ouverture } from 'app/shared/model/ouverture.model';

describe('Component Tests', () => {
    describe('Ouverture Management Component', () => {
        let comp: OuvertureComponent;
        let fixture: ComponentFixture<OuvertureComponent>;
        let service: OuvertureService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [MyrestoTestModule],
                declarations: [OuvertureComponent],
                providers: []
            })
                .overrideTemplate(OuvertureComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(OuvertureComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(OuvertureService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new Ouverture(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.ouvertures[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
