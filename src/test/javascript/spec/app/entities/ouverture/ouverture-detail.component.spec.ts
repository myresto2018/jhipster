/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MyrestoTestModule } from '../../../test.module';
import { OuvertureDetailComponent } from 'app/entities/ouverture/ouverture-detail.component';
import { Ouverture } from 'app/shared/model/ouverture.model';

describe('Component Tests', () => {
    describe('Ouverture Management Detail Component', () => {
        let comp: OuvertureDetailComponent;
        let fixture: ComponentFixture<OuvertureDetailComponent>;
        const route = ({ data: of({ ouverture: new Ouverture(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [MyrestoTestModule],
                declarations: [OuvertureDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(OuvertureDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(OuvertureDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.ouverture).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
