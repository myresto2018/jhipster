/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { MyrestoTestModule } from '../../../test.module';
import { OuvertureDeleteDialogComponent } from 'app/entities/ouverture/ouverture-delete-dialog.component';
import { OuvertureService } from 'app/entities/ouverture/ouverture.service';

describe('Component Tests', () => {
    describe('Ouverture Management Delete Component', () => {
        let comp: OuvertureDeleteDialogComponent;
        let fixture: ComponentFixture<OuvertureDeleteDialogComponent>;
        let service: OuvertureService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [MyrestoTestModule],
                declarations: [OuvertureDeleteDialogComponent]
            })
                .overrideTemplate(OuvertureDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(OuvertureDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(OuvertureService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it(
                'Should call delete service on confirmDelete',
                inject(
                    [],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });
});
