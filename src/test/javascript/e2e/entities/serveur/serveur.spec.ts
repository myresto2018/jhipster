/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { ServeurComponentsPage, ServeurDeleteDialog, ServeurUpdatePage } from './serveur.page-object';

const expect = chai.expect;

describe('Serveur e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let serveurUpdatePage: ServeurUpdatePage;
    let serveurComponentsPage: ServeurComponentsPage;
    let serveurDeleteDialog: ServeurDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Serveurs', async () => {
        await navBarPage.goToEntity('serveur');
        serveurComponentsPage = new ServeurComponentsPage();
        expect(await serveurComponentsPage.getTitle()).to.eq('myrestoApp.serveur.home.title');
    });

    it('should load create Serveur page', async () => {
        await serveurComponentsPage.clickOnCreateButton();
        serveurUpdatePage = new ServeurUpdatePage();
        expect(await serveurUpdatePage.getPageTitle()).to.eq('myrestoApp.serveur.home.createOrEditLabel');
        await serveurUpdatePage.cancel();
    });

    it('should create and save Serveurs', async () => {
        const nbButtonsBeforeCreate = await serveurComponentsPage.countDeleteButtons();

        await serveurComponentsPage.clickOnCreateButton();
        await promise.all([serveurUpdatePage.restaurantSelectLastOption()]);
        await serveurUpdatePage.save();
        expect(await serveurUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await serveurComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Serveur', async () => {
        const nbButtonsBeforeDelete = await serveurComponentsPage.countDeleteButtons();
        await serveurComponentsPage.clickOnLastDeleteButton();

        serveurDeleteDialog = new ServeurDeleteDialog();
        expect(await serveurDeleteDialog.getDialogTitle()).to.eq('myrestoApp.serveur.delete.question');
        await serveurDeleteDialog.clickOnConfirmButton();

        expect(await serveurComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
