/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { OuvertureComponentsPage, OuvertureDeleteDialog, OuvertureUpdatePage } from './ouverture.page-object';

const expect = chai.expect;

describe('Ouverture e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let ouvertureUpdatePage: OuvertureUpdatePage;
    let ouvertureComponentsPage: OuvertureComponentsPage;
    let ouvertureDeleteDialog: OuvertureDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Ouvertures', async () => {
        await navBarPage.goToEntity('ouverture');
        ouvertureComponentsPage = new OuvertureComponentsPage();
        expect(await ouvertureComponentsPage.getTitle()).to.eq('myrestoApp.ouverture.home.title');
    });

    it('should load create Ouverture page', async () => {
        await ouvertureComponentsPage.clickOnCreateButton();
        ouvertureUpdatePage = new OuvertureUpdatePage();
        expect(await ouvertureUpdatePage.getPageTitle()).to.eq('myrestoApp.ouverture.home.createOrEditLabel');
        await ouvertureUpdatePage.cancel();
    });

    it('should create and save Ouvertures', async () => {
        const nbButtonsBeforeCreate = await ouvertureComponentsPage.countDeleteButtons();

        await ouvertureComponentsPage.clickOnCreateButton();
        await promise.all([
            ouvertureUpdatePage.daySelectLastOption(),
            ouvertureUpdatePage.creneauSelectLastOption(),
            ouvertureUpdatePage.restaurantSelectLastOption()
        ]);
        await ouvertureUpdatePage.save();
        expect(await ouvertureUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await ouvertureComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Ouverture', async () => {
        const nbButtonsBeforeDelete = await ouvertureComponentsPage.countDeleteButtons();
        await ouvertureComponentsPage.clickOnLastDeleteButton();

        ouvertureDeleteDialog = new OuvertureDeleteDialog();
        expect(await ouvertureDeleteDialog.getDialogTitle()).to.eq('myrestoApp.ouverture.delete.question');
        await ouvertureDeleteDialog.clickOnConfirmButton();

        expect(await ouvertureComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
