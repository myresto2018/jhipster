import { element, by, ElementFinder } from 'protractor';

export class OuvertureComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-ouverture div table .btn-danger'));
    title = element.all(by.css('jhi-ouverture div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class OuvertureUpdatePage {
    pageTitle = element(by.id('jhi-ouverture-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    daySelect = element(by.id('field_day'));
    creneauSelect = element(by.id('field_creneau'));
    restaurantSelect = element(by.id('field_restaurant'));

    async getPageTitle() {
        return this.pageTitle.getAttribute('jhiTranslate');
    }

    async setDaySelect(day) {
        await this.daySelect.sendKeys(day);
    }

    async getDaySelect() {
        return this.daySelect.element(by.css('option:checked')).getText();
    }

    async daySelectLastOption() {
        await this.daySelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async setCreneauSelect(creneau) {
        await this.creneauSelect.sendKeys(creneau);
    }

    async getCreneauSelect() {
        return this.creneauSelect.element(by.css('option:checked')).getText();
    }

    async creneauSelectLastOption() {
        await this.creneauSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async restaurantSelectLastOption() {
        await this.restaurantSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async restaurantSelectOption(option) {
        await this.restaurantSelect.sendKeys(option);
    }

    getRestaurantSelect(): ElementFinder {
        return this.restaurantSelect;
    }

    async getRestaurantSelectedOption() {
        return this.restaurantSelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class OuvertureDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-ouverture-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-ouverture'));

    async getDialogTitle() {
        return this.dialogTitle.getAttribute('jhiTranslate');
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
