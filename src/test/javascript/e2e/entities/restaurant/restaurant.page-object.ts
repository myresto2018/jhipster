import { element, by, ElementFinder } from 'protractor';

export class RestaurantComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-restaurant div table .btn-danger'));
    title = element.all(by.css('jhi-restaurant div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class RestaurantUpdatePage {
    pageTitle = element(by.id('jhi-restaurant-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    nomInput = element(by.id('field_nom'));
    adresseInput = element(by.id('field_adresse'));
    capaciteInput = element(by.id('field_capacite'));
    latitudeInput = element(by.id('field_latitude'));
    longitudeInput = element(by.id('field_longitude'));
    nbtablesInput = element(by.id('field_nbtables'));
    carteSelect = element(by.id('field_carte'));

    async getPageTitle() {
        return this.pageTitle.getAttribute('jhiTranslate');
    }

    async setNomInput(nom) {
        await this.nomInput.sendKeys(nom);
    }

    async getNomInput() {
        return this.nomInput.getAttribute('value');
    }

    async setAdresseInput(adresse) {
        await this.adresseInput.sendKeys(adresse);
    }

    async getAdresseInput() {
        return this.adresseInput.getAttribute('value');
    }

    async setCapaciteInput(capacite) {
        await this.capaciteInput.sendKeys(capacite);
    }

    async getCapaciteInput() {
        return this.capaciteInput.getAttribute('value');
    }

    async setLatitudeInput(latitude) {
        await this.latitudeInput.sendKeys(latitude);
    }

    async getLatitudeInput() {
        return this.latitudeInput.getAttribute('value');
    }

    async setLongitudeInput(longitude) {
        await this.longitudeInput.sendKeys(longitude);
    }

    async getLongitudeInput() {
        return this.longitudeInput.getAttribute('value');
    }

    async setNbtablesInput(nbtables) {
        await this.nbtablesInput.sendKeys(nbtables);
    }

    async getNbtablesInput() {
        return this.nbtablesInput.getAttribute('value');
    }

    async carteSelectLastOption() {
        await this.carteSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async carteSelectOption(option) {
        await this.carteSelect.sendKeys(option);
    }

    getCarteSelect(): ElementFinder {
        return this.carteSelect;
    }

    async getCarteSelectedOption() {
        return this.carteSelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class RestaurantDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-restaurant-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-restaurant'));

    async getDialogTitle() {
        return this.dialogTitle.getAttribute('jhiTranslate');
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
