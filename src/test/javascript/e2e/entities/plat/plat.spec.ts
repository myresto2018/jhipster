/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { PlatComponentsPage, PlatDeleteDialog, PlatUpdatePage } from './plat.page-object';

const expect = chai.expect;

describe('Plat e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let platUpdatePage: PlatUpdatePage;
    let platComponentsPage: PlatComponentsPage;
    let platDeleteDialog: PlatDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Plats', async () => {
        await navBarPage.goToEntity('plat');
        platComponentsPage = new PlatComponentsPage();
        expect(await platComponentsPage.getTitle()).to.eq('myrestoApp.plat.home.title');
    });

    it('should load create Plat page', async () => {
        await platComponentsPage.clickOnCreateButton();
        platUpdatePage = new PlatUpdatePage();
        expect(await platUpdatePage.getPageTitle()).to.eq('myrestoApp.plat.home.createOrEditLabel');
        await platUpdatePage.cancel();
    });

    it('should create and save Plats', async () => {
        const nbButtonsBeforeCreate = await platComponentsPage.countDeleteButtons();

        await platComponentsPage.clickOnCreateButton();
        await promise.all([
            platUpdatePage.setNomInput('nom'),
            platUpdatePage.setDescriptionInput('description'),
            platUpdatePage.typeSelectLastOption(),
            platUpdatePage.setPrixInput('5'),
            platUpdatePage.carteSelectLastOption()
        ]);
        expect(await platUpdatePage.getNomInput()).to.eq('nom');
        expect(await platUpdatePage.getDescriptionInput()).to.eq('description');
        expect(await platUpdatePage.getPrixInput()).to.eq('5');
        await platUpdatePage.save();
        expect(await platUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await platComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Plat', async () => {
        const nbButtonsBeforeDelete = await platComponentsPage.countDeleteButtons();
        await platComponentsPage.clickOnLastDeleteButton();

        platDeleteDialog = new PlatDeleteDialog();
        expect(await platDeleteDialog.getDialogTitle()).to.eq('myrestoApp.plat.delete.question');
        await platDeleteDialog.clickOnConfirmButton();

        expect(await platComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
