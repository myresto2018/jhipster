import { element, by, ElementFinder } from 'protractor';

export class PlatComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-plat div table .btn-danger'));
    title = element.all(by.css('jhi-plat div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class PlatUpdatePage {
    pageTitle = element(by.id('jhi-plat-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    nomInput = element(by.id('field_nom'));
    descriptionInput = element(by.id('field_description'));
    typeSelect = element(by.id('field_type'));
    prixInput = element(by.id('field_prix'));
    carteSelect = element(by.id('field_carte'));

    async getPageTitle() {
        return this.pageTitle.getAttribute('jhiTranslate');
    }

    async setNomInput(nom) {
        await this.nomInput.sendKeys(nom);
    }

    async getNomInput() {
        return this.nomInput.getAttribute('value');
    }

    async setDescriptionInput(description) {
        await this.descriptionInput.sendKeys(description);
    }

    async getDescriptionInput() {
        return this.descriptionInput.getAttribute('value');
    }

    async setTypeSelect(type) {
        await this.typeSelect.sendKeys(type);
    }

    async getTypeSelect() {
        return this.typeSelect.element(by.css('option:checked')).getText();
    }

    async typeSelectLastOption() {
        await this.typeSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async setPrixInput(prix) {
        await this.prixInput.sendKeys(prix);
    }

    async getPrixInput() {
        return this.prixInput.getAttribute('value');
    }

    async carteSelectLastOption() {
        await this.carteSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async carteSelectOption(option) {
        await this.carteSelect.sendKeys(option);
    }

    getCarteSelect(): ElementFinder {
        return this.carteSelect;
    }

    async getCarteSelectedOption() {
        return this.carteSelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class PlatDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-plat-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-plat'));

    async getDialogTitle() {
        return this.dialogTitle.getAttribute('jhiTranslate');
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
