/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { CarteComponentsPage, CarteDeleteDialog, CarteUpdatePage } from './carte.page-object';

const expect = chai.expect;

describe('Carte e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let carteUpdatePage: CarteUpdatePage;
    let carteComponentsPage: CarteComponentsPage;
    let carteDeleteDialog: CarteDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Cartes', async () => {
        await navBarPage.goToEntity('carte');
        carteComponentsPage = new CarteComponentsPage();
        expect(await carteComponentsPage.getTitle()).to.eq('myrestoApp.carte.home.title');
    });

    it('should load create Carte page', async () => {
        await carteComponentsPage.clickOnCreateButton();
        carteUpdatePage = new CarteUpdatePage();
        expect(await carteUpdatePage.getPageTitle()).to.eq('myrestoApp.carte.home.createOrEditLabel');
        await carteUpdatePage.cancel();
    });

    it('should create and save Cartes', async () => {
        const nbButtonsBeforeCreate = await carteComponentsPage.countDeleteButtons();

        await carteComponentsPage.clickOnCreateButton();
        await promise.all([]);
        await carteUpdatePage.save();
        expect(await carteUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await carteComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Carte', async () => {
        const nbButtonsBeforeDelete = await carteComponentsPage.countDeleteButtons();
        await carteComponentsPage.clickOnLastDeleteButton();

        carteDeleteDialog = new CarteDeleteDialog();
        expect(await carteDeleteDialog.getDialogTitle()).to.eq('myrestoApp.carte.delete.question');
        await carteDeleteDialog.clickOnConfirmButton();

        expect(await carteComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
