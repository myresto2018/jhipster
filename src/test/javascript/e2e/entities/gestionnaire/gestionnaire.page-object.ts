import { element, by, ElementFinder } from 'protractor';

export class GestionnaireComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-gestionnaire div table .btn-danger'));
    title = element.all(by.css('jhi-gestionnaire div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class GestionnaireUpdatePage {
    pageTitle = element(by.id('jhi-gestionnaire-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    restaurantSelect = element(by.id('field_restaurant'));
    superviseurSelect = element(by.id('field_superviseur'));

    async getPageTitle() {
        return this.pageTitle.getAttribute('jhiTranslate');
    }

    async restaurantSelectLastOption() {
        await this.restaurantSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async restaurantSelectOption(option) {
        await this.restaurantSelect.sendKeys(option);
    }

    getRestaurantSelect(): ElementFinder {
        return this.restaurantSelect;
    }

    async getRestaurantSelectedOption() {
        return this.restaurantSelect.element(by.css('option:checked')).getText();
    }

    async superviseurSelectLastOption() {
        await this.superviseurSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async superviseurSelectOption(option) {
        await this.superviseurSelect.sendKeys(option);
    }

    getSuperviseurSelect(): ElementFinder {
        return this.superviseurSelect;
    }

    async getSuperviseurSelectedOption() {
        return this.superviseurSelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class GestionnaireDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-gestionnaire-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-gestionnaire'));

    async getDialogTitle() {
        return this.dialogTitle.getAttribute('jhiTranslate');
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
