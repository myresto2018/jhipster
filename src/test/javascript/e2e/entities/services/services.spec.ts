/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { ServicesComponentsPage, ServicesDeleteDialog, ServicesUpdatePage } from './services.page-object';

const expect = chai.expect;

describe('Services e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let servicesUpdatePage: ServicesUpdatePage;
    let servicesComponentsPage: ServicesComponentsPage;
    let servicesDeleteDialog: ServicesDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Services', async () => {
        await navBarPage.goToEntity('services');
        servicesComponentsPage = new ServicesComponentsPage();
        expect(await servicesComponentsPage.getTitle()).to.eq('myrestoApp.services.home.title');
    });

    it('should load create Services page', async () => {
        await servicesComponentsPage.clickOnCreateButton();
        servicesUpdatePage = new ServicesUpdatePage();
        expect(await servicesUpdatePage.getPageTitle()).to.eq('myrestoApp.services.home.createOrEditLabel');
        await servicesUpdatePage.cancel();
    });

    it('should create and save Services', async () => {
        const nbButtonsBeforeCreate = await servicesComponentsPage.countDeleteButtons();

        await servicesComponentsPage.clickOnCreateButton();
        await promise.all([
            servicesUpdatePage.setDateInput('2000-12-31'),
            servicesUpdatePage.horaireSelectLastOption(),
            servicesUpdatePage.restaurantSelectLastOption()
        ]);
        expect(await servicesUpdatePage.getDateInput()).to.eq('2000-12-31');
        await servicesUpdatePage.save();
        expect(await servicesUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await servicesComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Services', async () => {
        const nbButtonsBeforeDelete = await servicesComponentsPage.countDeleteButtons();
        await servicesComponentsPage.clickOnLastDeleteButton();

        servicesDeleteDialog = new ServicesDeleteDialog();
        expect(await servicesDeleteDialog.getDialogTitle()).to.eq('myrestoApp.services.delete.question');
        await servicesDeleteDialog.clickOnConfirmButton();

        expect(await servicesComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
