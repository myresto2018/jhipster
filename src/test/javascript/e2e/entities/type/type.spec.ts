/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { TypeComponentsPage, TypeDeleteDialog, TypeUpdatePage } from './type.page-object';

const expect = chai.expect;

describe('Type e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let typeUpdatePage: TypeUpdatePage;
    let typeComponentsPage: TypeComponentsPage;
    let typeDeleteDialog: TypeDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Types', async () => {
        await navBarPage.goToEntity('type');
        typeComponentsPage = new TypeComponentsPage();
        expect(await typeComponentsPage.getTitle()).to.eq('myrestoApp.type.home.title');
    });

    it('should load create Type page', async () => {
        await typeComponentsPage.clickOnCreateButton();
        typeUpdatePage = new TypeUpdatePage();
        expect(await typeUpdatePage.getPageTitle()).to.eq('myrestoApp.type.home.createOrEditLabel');
        await typeUpdatePage.cancel();
    });

    it('should create and save Types', async () => {
        const nbButtonsBeforeCreate = await typeComponentsPage.countDeleteButtons();

        await typeComponentsPage.clickOnCreateButton();
        await promise.all([typeUpdatePage.typeuSelectLastOption(), typeUpdatePage.restaurantSelectLastOption()]);
        await typeUpdatePage.save();
        expect(await typeUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await typeComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Type', async () => {
        const nbButtonsBeforeDelete = await typeComponentsPage.countDeleteButtons();
        await typeComponentsPage.clickOnLastDeleteButton();

        typeDeleteDialog = new TypeDeleteDialog();
        expect(await typeDeleteDialog.getDialogTitle()).to.eq('myrestoApp.type.delete.question');
        await typeDeleteDialog.clickOnConfirmButton();

        expect(await typeComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
