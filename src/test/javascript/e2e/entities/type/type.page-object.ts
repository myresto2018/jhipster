import { element, by, ElementFinder } from 'protractor';

export class TypeComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-type div table .btn-danger'));
    title = element.all(by.css('jhi-type div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class TypeUpdatePage {
    pageTitle = element(by.id('jhi-type-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    typeuSelect = element(by.id('field_typeu'));
    restaurantSelect = element(by.id('field_restaurant'));

    async getPageTitle() {
        return this.pageTitle.getAttribute('jhiTranslate');
    }

    async setTypeuSelect(typeu) {
        await this.typeuSelect.sendKeys(typeu);
    }

    async getTypeuSelect() {
        return this.typeuSelect.element(by.css('option:checked')).getText();
    }

    async typeuSelectLastOption() {
        await this.typeuSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async restaurantSelectLastOption() {
        await this.restaurantSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async restaurantSelectOption(option) {
        await this.restaurantSelect.sendKeys(option);
    }

    getRestaurantSelect(): ElementFinder {
        return this.restaurantSelect;
    }

    async getRestaurantSelectedOption() {
        return this.restaurantSelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class TypeDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-type-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-type'));

    async getDialogTitle() {
        return this.dialogTitle.getAttribute('jhiTranslate');
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
