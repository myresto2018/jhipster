import { element, by, ElementFinder } from 'protractor';

export class ReservationComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-reservation div table .btn-danger'));
    title = element.all(by.css('jhi-reservation div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class ReservationUpdatePage {
    pageTitle = element(by.id('jhi-reservation-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    nbCouvertsInput = element(by.id('field_nbCouverts'));
    servicesSelect = element(by.id('field_services'));
    tablesSelect = element(by.id('field_tables'));
    serveurSelect = element(by.id('field_serveur'));
    clientSelect = element(by.id('field_client'));

    async getPageTitle() {
        return this.pageTitle.getAttribute('jhiTranslate');
    }

    async setNbCouvertsInput(nbCouverts) {
        await this.nbCouvertsInput.sendKeys(nbCouverts);
    }

    async getNbCouvertsInput() {
        return this.nbCouvertsInput.getAttribute('value');
    }

    async servicesSelectLastOption() {
        await this.servicesSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async servicesSelectOption(option) {
        await this.servicesSelect.sendKeys(option);
    }

    getServicesSelect(): ElementFinder {
        return this.servicesSelect;
    }

    async getServicesSelectedOption() {
        return this.servicesSelect.element(by.css('option:checked')).getText();
    }

    async tablesSelectLastOption() {
        await this.tablesSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async tablesSelectOption(option) {
        await this.tablesSelect.sendKeys(option);
    }

    getTablesSelect(): ElementFinder {
        return this.tablesSelect;
    }

    async getTablesSelectedOption() {
        return this.tablesSelect.element(by.css('option:checked')).getText();
    }

    async serveurSelectLastOption() {
        await this.serveurSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async serveurSelectOption(option) {
        await this.serveurSelect.sendKeys(option);
    }

    getServeurSelect(): ElementFinder {
        return this.serveurSelect;
    }

    async getServeurSelectedOption() {
        return this.serveurSelect.element(by.css('option:checked')).getText();
    }

    async clientSelectLastOption() {
        await this.clientSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async clientSelectOption(option) {
        await this.clientSelect.sendKeys(option);
    }

    getClientSelect(): ElementFinder {
        return this.clientSelect;
    }

    async getClientSelectedOption() {
        return this.clientSelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class ReservationDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-reservation-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-reservation'));

    async getDialogTitle() {
        return this.dialogTitle.getAttribute('jhiTranslate');
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
