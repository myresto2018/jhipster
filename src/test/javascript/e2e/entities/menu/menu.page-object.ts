import { element, by, ElementFinder } from 'protractor';

export class MenuComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-menu div table .btn-danger'));
    title = element.all(by.css('jhi-menu div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class MenuUpdatePage {
    pageTitle = element(by.id('jhi-menu-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    nomInput = element(by.id('field_nom'));
    descriptionInput = element(by.id('field_description'));
    priceInput = element(by.id('field_price'));
    platSelect = element(by.id('field_plat'));
    carteSelect = element(by.id('field_carte'));

    async getPageTitle() {
        return this.pageTitle.getAttribute('jhiTranslate');
    }

    async setNomInput(nom) {
        await this.nomInput.sendKeys(nom);
    }

    async getNomInput() {
        return this.nomInput.getAttribute('value');
    }

    async setDescriptionInput(description) {
        await this.descriptionInput.sendKeys(description);
    }

    async getDescriptionInput() {
        return this.descriptionInput.getAttribute('value');
    }

    async setPriceInput(price) {
        await this.priceInput.sendKeys(price);
    }

    async getPriceInput() {
        return this.priceInput.getAttribute('value');
    }

    async platSelectLastOption() {
        await this.platSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async platSelectOption(option) {
        await this.platSelect.sendKeys(option);
    }

    getPlatSelect(): ElementFinder {
        return this.platSelect;
    }

    async getPlatSelectedOption() {
        return this.platSelect.element(by.css('option:checked')).getText();
    }

    async carteSelectLastOption() {
        await this.carteSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async carteSelectOption(option) {
        await this.carteSelect.sendKeys(option);
    }

    getCarteSelect(): ElementFinder {
        return this.carteSelect;
    }

    async getCarteSelectedOption() {
        return this.carteSelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class MenuDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-menu-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-menu'));

    async getDialogTitle() {
        return this.dialogTitle.getAttribute('jhiTranslate');
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
