package ecom.myresto.web.rest;

import ecom.myresto.MyrestoApp;

import ecom.myresto.domain.Tables;
import ecom.myresto.repository.TablesRepository;
import ecom.myresto.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static ecom.myresto.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TablesResource REST controller.
 *
 * @see TablesResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MyrestoApp.class)
public class TablesResourceIntTest {

    private static final Integer DEFAULT_X = 1;
    private static final Integer UPDATED_X = 2;

    private static final Integer DEFAULT_Y = 1;
    private static final Integer UPDATED_Y = 2;

    @Autowired
    private TablesRepository tablesRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTablesMockMvc;

    private Tables tables;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TablesResource tablesResource = new TablesResource(tablesRepository);
        this.restTablesMockMvc = MockMvcBuilders.standaloneSetup(tablesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Tables createEntity(EntityManager em) {
        Tables tables = new Tables()
            .x(DEFAULT_X)
            .y(DEFAULT_Y);
        return tables;
    }

    @Before
    public void initTest() {
        tables = createEntity(em);
    }

    @Test
    @Transactional
    public void createTables() throws Exception {
        int databaseSizeBeforeCreate = tablesRepository.findAll().size();

        // Create the Tables
        restTablesMockMvc.perform(post("/api/tables")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tables)))
            .andExpect(status().isCreated());

        // Validate the Tables in the database
        List<Tables> tablesList = tablesRepository.findAll();
        assertThat(tablesList).hasSize(databaseSizeBeforeCreate + 1);
        Tables testTables = tablesList.get(tablesList.size() - 1);
        assertThat(testTables.getX()).isEqualTo(DEFAULT_X);
        assertThat(testTables.getY()).isEqualTo(DEFAULT_Y);
    }

    @Test
    @Transactional
    public void createTablesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tablesRepository.findAll().size();

        // Create the Tables with an existing ID
        tables.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTablesMockMvc.perform(post("/api/tables")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tables)))
            .andExpect(status().isBadRequest());

        // Validate the Tables in the database
        List<Tables> tablesList = tablesRepository.findAll();
        assertThat(tablesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkXIsRequired() throws Exception {
        int databaseSizeBeforeTest = tablesRepository.findAll().size();
        // set the field null
        tables.setX(null);

        // Create the Tables, which fails.

        restTablesMockMvc.perform(post("/api/tables")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tables)))
            .andExpect(status().isBadRequest());

        List<Tables> tablesList = tablesRepository.findAll();
        assertThat(tablesList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkYIsRequired() throws Exception {
        int databaseSizeBeforeTest = tablesRepository.findAll().size();
        // set the field null
        tables.setY(null);

        // Create the Tables, which fails.

        restTablesMockMvc.perform(post("/api/tables")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tables)))
            .andExpect(status().isBadRequest());

        List<Tables> tablesList = tablesRepository.findAll();
        assertThat(tablesList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTables() throws Exception {
        // Initialize the database
        tablesRepository.saveAndFlush(tables);

        // Get all the tablesList
        restTablesMockMvc.perform(get("/api/tables?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tables.getId().intValue())))
            .andExpect(jsonPath("$.[*].x").value(hasItem(DEFAULT_X)))
            .andExpect(jsonPath("$.[*].y").value(hasItem(DEFAULT_Y)));
    }
    
    @Test
    @Transactional
    public void getTables() throws Exception {
        // Initialize the database
        tablesRepository.saveAndFlush(tables);

        // Get the tables
        restTablesMockMvc.perform(get("/api/tables/{id}", tables.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tables.getId().intValue()))
            .andExpect(jsonPath("$.x").value(DEFAULT_X))
            .andExpect(jsonPath("$.y").value(DEFAULT_Y));
    }

    @Test
    @Transactional
    public void getNonExistingTables() throws Exception {
        // Get the tables
        restTablesMockMvc.perform(get("/api/tables/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTables() throws Exception {
        // Initialize the database
        tablesRepository.saveAndFlush(tables);

        int databaseSizeBeforeUpdate = tablesRepository.findAll().size();

        // Update the tables
        Tables updatedTables = tablesRepository.findById(tables.getId()).get();
        // Disconnect from session so that the updates on updatedTables are not directly saved in db
        em.detach(updatedTables);
        updatedTables
            .x(UPDATED_X)
            .y(UPDATED_Y);

        restTablesMockMvc.perform(put("/api/tables")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTables)))
            .andExpect(status().isOk());

        // Validate the Tables in the database
        List<Tables> tablesList = tablesRepository.findAll();
        assertThat(tablesList).hasSize(databaseSizeBeforeUpdate);
        Tables testTables = tablesList.get(tablesList.size() - 1);
        assertThat(testTables.getX()).isEqualTo(UPDATED_X);
        assertThat(testTables.getY()).isEqualTo(UPDATED_Y);
    }

    @Test
    @Transactional
    public void updateNonExistingTables() throws Exception {
        int databaseSizeBeforeUpdate = tablesRepository.findAll().size();

        // Create the Tables

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTablesMockMvc.perform(put("/api/tables")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tables)))
            .andExpect(status().isBadRequest());

        // Validate the Tables in the database
        List<Tables> tablesList = tablesRepository.findAll();
        assertThat(tablesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTables() throws Exception {
        // Initialize the database
        tablesRepository.saveAndFlush(tables);

        int databaseSizeBeforeDelete = tablesRepository.findAll().size();

        // Get the tables
        restTablesMockMvc.perform(delete("/api/tables/{id}", tables.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Tables> tablesList = tablesRepository.findAll();
        assertThat(tablesList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Tables.class);
        Tables tables1 = new Tables();
        tables1.setId(1L);
        Tables tables2 = new Tables();
        tables2.setId(tables1.getId());
        assertThat(tables1).isEqualTo(tables2);
        tables2.setId(2L);
        assertThat(tables1).isNotEqualTo(tables2);
        tables1.setId(null);
        assertThat(tables1).isNotEqualTo(tables2);
    }
}
