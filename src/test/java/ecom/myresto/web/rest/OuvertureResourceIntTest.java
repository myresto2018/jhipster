package ecom.myresto.web.rest;

import ecom.myresto.MyrestoApp;

import ecom.myresto.domain.Ouverture;
import ecom.myresto.repository.OuvertureRepository;
import ecom.myresto.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static ecom.myresto.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import ecom.myresto.domain.enumeration.Jour;
import ecom.myresto.domain.enumeration.HoraireService;
/**
 * Test class for the OuvertureResource REST controller.
 *
 * @see OuvertureResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MyrestoApp.class)
public class OuvertureResourceIntTest {

    private static final Jour DEFAULT_DAY = Jour.LUNDI;
    private static final Jour UPDATED_DAY = Jour.MARDI;

    private static final HoraireService DEFAULT_CRENEAU = HoraireService.MATIN;
    private static final HoraireService UPDATED_CRENEAU = HoraireService.MIDI;

    @Autowired
    private OuvertureRepository ouvertureRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restOuvertureMockMvc;

    private Ouverture ouverture;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final OuvertureResource ouvertureResource = new OuvertureResource(ouvertureRepository);
        this.restOuvertureMockMvc = MockMvcBuilders.standaloneSetup(ouvertureResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Ouverture createEntity(EntityManager em) {
        Ouverture ouverture = new Ouverture()
            .day(DEFAULT_DAY)
            .creneau(DEFAULT_CRENEAU);
        return ouverture;
    }

    @Before
    public void initTest() {
        ouverture = createEntity(em);
    }

    @Test
    @Transactional
    public void createOuverture() throws Exception {
        int databaseSizeBeforeCreate = ouvertureRepository.findAll().size();

        // Create the Ouverture
        restOuvertureMockMvc.perform(post("/api/ouvertures")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ouverture)))
            .andExpect(status().isCreated());

        // Validate the Ouverture in the database
        List<Ouverture> ouvertureList = ouvertureRepository.findAll();
        assertThat(ouvertureList).hasSize(databaseSizeBeforeCreate + 1);
        Ouverture testOuverture = ouvertureList.get(ouvertureList.size() - 1);
        assertThat(testOuverture.getDay()).isEqualTo(DEFAULT_DAY);
        assertThat(testOuverture.getCreneau()).isEqualTo(DEFAULT_CRENEAU);
    }

    @Test
    @Transactional
    public void createOuvertureWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = ouvertureRepository.findAll().size();

        // Create the Ouverture with an existing ID
        ouverture.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOuvertureMockMvc.perform(post("/api/ouvertures")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ouverture)))
            .andExpect(status().isBadRequest());

        // Validate the Ouverture in the database
        List<Ouverture> ouvertureList = ouvertureRepository.findAll();
        assertThat(ouvertureList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDayIsRequired() throws Exception {
        int databaseSizeBeforeTest = ouvertureRepository.findAll().size();
        // set the field null
        ouverture.setDay(null);

        // Create the Ouverture, which fails.

        restOuvertureMockMvc.perform(post("/api/ouvertures")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ouverture)))
            .andExpect(status().isBadRequest());

        List<Ouverture> ouvertureList = ouvertureRepository.findAll();
        assertThat(ouvertureList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCreneauIsRequired() throws Exception {
        int databaseSizeBeforeTest = ouvertureRepository.findAll().size();
        // set the field null
        ouverture.setCreneau(null);

        // Create the Ouverture, which fails.

        restOuvertureMockMvc.perform(post("/api/ouvertures")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ouverture)))
            .andExpect(status().isBadRequest());

        List<Ouverture> ouvertureList = ouvertureRepository.findAll();
        assertThat(ouvertureList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllOuvertures() throws Exception {
        // Initialize the database
        ouvertureRepository.saveAndFlush(ouverture);

        // Get all the ouvertureList
        restOuvertureMockMvc.perform(get("/api/ouvertures?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(ouverture.getId().intValue())))
            .andExpect(jsonPath("$.[*].day").value(hasItem(DEFAULT_DAY.toString())))
            .andExpect(jsonPath("$.[*].creneau").value(hasItem(DEFAULT_CRENEAU.toString())));
    }
    
    @Test
    @Transactional
    public void getOuverture() throws Exception {
        // Initialize the database
        ouvertureRepository.saveAndFlush(ouverture);

        // Get the ouverture
        restOuvertureMockMvc.perform(get("/api/ouvertures/{id}", ouverture.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(ouverture.getId().intValue()))
            .andExpect(jsonPath("$.day").value(DEFAULT_DAY.toString()))
            .andExpect(jsonPath("$.creneau").value(DEFAULT_CRENEAU.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingOuverture() throws Exception {
        // Get the ouverture
        restOuvertureMockMvc.perform(get("/api/ouvertures/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOuverture() throws Exception {
        // Initialize the database
        ouvertureRepository.saveAndFlush(ouverture);

        int databaseSizeBeforeUpdate = ouvertureRepository.findAll().size();

        // Update the ouverture
        Ouverture updatedOuverture = ouvertureRepository.findById(ouverture.getId()).get();
        // Disconnect from session so that the updates on updatedOuverture are not directly saved in db
        em.detach(updatedOuverture);
        updatedOuverture
            .day(UPDATED_DAY)
            .creneau(UPDATED_CRENEAU);

        restOuvertureMockMvc.perform(put("/api/ouvertures")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedOuverture)))
            .andExpect(status().isOk());

        // Validate the Ouverture in the database
        List<Ouverture> ouvertureList = ouvertureRepository.findAll();
        assertThat(ouvertureList).hasSize(databaseSizeBeforeUpdate);
        Ouverture testOuverture = ouvertureList.get(ouvertureList.size() - 1);
        assertThat(testOuverture.getDay()).isEqualTo(UPDATED_DAY);
        assertThat(testOuverture.getCreneau()).isEqualTo(UPDATED_CRENEAU);
    }

    @Test
    @Transactional
    public void updateNonExistingOuverture() throws Exception {
        int databaseSizeBeforeUpdate = ouvertureRepository.findAll().size();

        // Create the Ouverture

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOuvertureMockMvc.perform(put("/api/ouvertures")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ouverture)))
            .andExpect(status().isBadRequest());

        // Validate the Ouverture in the database
        List<Ouverture> ouvertureList = ouvertureRepository.findAll();
        assertThat(ouvertureList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteOuverture() throws Exception {
        // Initialize the database
        ouvertureRepository.saveAndFlush(ouverture);

        int databaseSizeBeforeDelete = ouvertureRepository.findAll().size();

        // Get the ouverture
        restOuvertureMockMvc.perform(delete("/api/ouvertures/{id}", ouverture.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Ouverture> ouvertureList = ouvertureRepository.findAll();
        assertThat(ouvertureList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Ouverture.class);
        Ouverture ouverture1 = new Ouverture();
        ouverture1.setId(1L);
        Ouverture ouverture2 = new Ouverture();
        ouverture2.setId(ouverture1.getId());
        assertThat(ouverture1).isEqualTo(ouverture2);
        ouverture2.setId(2L);
        assertThat(ouverture1).isNotEqualTo(ouverture2);
        ouverture1.setId(null);
        assertThat(ouverture1).isNotEqualTo(ouverture2);
    }
}
